-- MySQL dump 10.13  Distrib 8.0.14, for Win64 (x86_64)
--
-- Host: localhost    Database: farmers
-- ------------------------------------------------------
-- Server version	8.0.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `branch` (
  `branch_id` int(11) DEFAULT NULL,
  `branch_code` varchar(50) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`branch_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `country` (
  `country_name` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `country_code` varchar(100) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_code`),
  UNIQUE KEY `country1_un` (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crop`
--

DROP TABLE IF EXISTS `crop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `crop` (
  `crop_code` varchar(100) NOT NULL,
  `crop_name` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`crop_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `district`
--

DROP TABLE IF EXISTS `district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `district` (
  `district_id` int(11) NOT NULL,
  `district_code` varchar(100) NOT NULL,
  `district_name` varchar(100) NOT NULL,
  `state_code` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`district_code`),
  KEY `district_state_fk` (`state_code`),
  CONSTRAINT `district_state_fk` FOREIGN KEY (`state_code`) REFERENCES `state` (`state_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `farmer`
--

DROP TABLE IF EXISTS `farmer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `farmer` (
  `farmer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `mobile_no` varchar(12) DEFAULT NULL,
  `branch_code` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  `taluka_code` varchar(100) NOT NULL,
  `village_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`farmer_id`),
  KEY `farmer_branch_fk` (`branch_code`),
  KEY `farmer_taluka_fk` (`taluka_code`),
  CONSTRAINT `farmer_branch_fk` FOREIGN KEY (`branch_code`) REFERENCES `branch` (`branch_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `farmer_taluka_fk` FOREIGN KEY (`taluka_code`) REFERENCES `taluka` (`taluka_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `farmer_stg`
--

DROP TABLE IF EXISTS `farmer_stg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `farmer_stg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_no` varchar(45) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `village_name` varchar(100) DEFAULT NULL,
  `tq` varchar(100) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `win_branch` varchar(45) DEFAULT NULL,
  `branch` varchar(45) DEFAULT NULL,
  `digit` varchar(45) DEFAULT NULL,
  `log` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `farmer_varient_mapping`
--

DROP TABLE IF EXISTS `farmer_varient_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `farmer_varient_mapping` (
  `farmer_id` int(11) NOT NULL,
  `varient_code` varchar(100) NOT NULL,
  `farmer_varient_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`farmer_varient_id`),
  KEY `farmer_varient_mapping_farmer_fk` (`farmer_id`),
  KEY `farmer_varient_mapping_varient_fk` (`varient_code`),
  CONSTRAINT `farmer_varient_mapping_farmer_fk` FOREIGN KEY (`farmer_id`) REFERENCES `farmer` (`farmer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `farmer_varient_mapping_varient_fk` FOREIGN KEY (`varient_code`) REFERENCES `varient` (`varient_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mast_role_modules`
--

DROP TABLE IF EXISTS `mast_role_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mast_role_modules` (
  `ROLE_FUNC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` int(11) DEFAULT NULL,
  `MODULE_ID` int(11) DEFAULT NULL,
  `CREATE_FLAG` varchar(1) DEFAULT NULL,
  `READ_FLAG` varchar(1) DEFAULT NULL,
  `EDIT_FLAG` varchar(1) DEFAULT NULL,
  `DELETE_FLAG` varchar(1) DEFAULT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATION_DATE` date NOT NULL,
  `LAST_UPDATE_LOGIN` int(11) NOT NULL,
  `LAST_UPDATED_BY` int(11) DEFAULT NULL,
  `LAST_UPDATE_DATE` date DEFAULT NULL,
  `UPDATEFLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ROLE_FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_modules`
--

DROP TABLE IF EXISTS `mst_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mst_modules` (
  `MODULE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_NAME` varchar(80) NOT NULL,
  `MODULE_DESC` varchar(240) DEFAULT NULL,
  `MODULE_URL` varchar(240) NOT NULL,
  `MODULE_GROUP` varchar(80) DEFAULT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATION_DATE` date NOT NULL,
  `LAST_UPDATE_LOGIN` int(11) NOT NULL,
  `LAST_UPDATED_BY` int(11) DEFAULT NULL,
  `LAST_UPDATE_DATE` date DEFAULT NULL,
  `MODULE_CODE` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`MODULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_roles`
--

DROP TABLE IF EXISTS `mst_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mst_roles` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_NAME` varchar(100) DEFAULT NULL,
  `ROLE_DESC` varchar(240) DEFAULT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATION_DATE` date NOT NULL,
  `LAST_UPDATE_LOGIN` int(11) NOT NULL,
  `LAST_UPDATED_BY` int(11) DEFAULT NULL,
  `LAST_UPDATE_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_user_roles`
--

DROP TABLE IF EXISTS `mst_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mst_user_roles` (
  `USER_ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `ROLE_ID` int(11) DEFAULT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATION_DATE` date NOT NULL,
  `LAST_UPDATE_LOGIN` int(11) NOT NULL,
  `LAST_UPDATED_BY` int(11) DEFAULT NULL,
  `LAST_UPDATE_DATE` date DEFAULT NULL,
  `UPDATE_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`USER_ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mst_users`
--

DROP TABLE IF EXISTS `mst_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mst_users` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(80) NOT NULL,
  `EMAIL` varchar(240) DEFAULT NULL,
  `PASSWORD` varchar(240) DEFAULT NULL,
  `USER_TYPE` varchar(240) DEFAULT NULL,
  `STATUS` varchar(80) DEFAULT NULL,
  `DESCRIPTION` varchar(240) DEFAULT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date DEFAULT NULL,
  `CHANGE_PWD` varchar(1) DEFAULT NULL,
  `PASSWORD_DATE` date DEFAULT NULL,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  `LOGINFAILUREATTEMPT` int(11) DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATION_DATE` date NOT NULL,
  `LAST_UPDATE_LOGIN` int(11) NOT NULL,
  `LAST_UPDATED_BY` int(11) DEFAULT NULL,
  `LAST_UPDATE_DATE` date DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `state` (
  `state_id` int(11) DEFAULT NULL,
  `state_code` varchar(100) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `country_code` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`state_code`),
  KEY `state_country1_fk` (`country_code`),
  CONSTRAINT `state_country1_fk` FOREIGN KEY (`country_code`) REFERENCES `country` (`country_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taluka`
--

DROP TABLE IF EXISTS `taluka`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `taluka` (
  `taluka_id` int(11) NOT NULL,
  `taluka_code` varchar(100) NOT NULL,
  `taluka_name` varchar(100) NOT NULL,
  `district_code` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`taluka_code`),
  KEY `taluka_district_fk` (`district_code`),
  CONSTRAINT `taluka_district_fk` FOREIGN KEY (`district_code`) REFERENCES `district` (`district_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `varient`
--

DROP TABLE IF EXISTS `varient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `varient` (
  `varient_id` int(11) DEFAULT NULL,
  `varient_code` varchar(100) NOT NULL,
  `varient_name` varchar(100) NOT NULL,
  `crop_code` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`varient_code`),
  KEY `varient_crop_fk` (`crop_code`),
  CONSTRAINT `varient_crop_fk` FOREIGN KEY (`crop_code`) REFERENCES `crop` (`crop_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `village`
--

DROP TABLE IF EXISTS `village`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `village` (
  `village_id` int(11) NOT NULL,
  `village_code` varchar(100) NOT NULL,
  `village_name` varchar(100) NOT NULL,
  `taluka_code` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`village_code`),
  KEY `village_taluka_fk` (`taluka_code`),
  CONSTRAINT `village_taluka_fk` FOREIGN KEY (`taluka_code`) REFERENCES `taluka` (`taluka_code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-29  9:14:57
