package com.nath.bo;
// Generated Apr 29, 2019 11:08:41 PM by Hibernate Tools 5.1.10.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MstRoles generated by hbm2java
 */
@Entity
@Table(name = "mst_roles" )
public class MstRoles implements java.io.Serializable {

	private Integer roleId;
	private String roleName;
	private String roleDesc;
	private String status;
	private Date startDate;
	private Date endDate;
	private int createdBy;
	private Date creationDate;
	private int lastUpdateLogin;
	private Integer lastUpdatedBy;
	private Date lastUpdateDate;
	private Boolean isDeleted;
	private Set<MstUserRoles> mstUserRoleses = new HashSet<MstUserRoles>(0);
	private Set<MastRoleModules> mastRoleModuleses = new HashSet<MastRoleModules>(0);

	public MstRoles() {
		
	}

	public MstRoles(Date startDate, int createdBy, Date creationDate, int lastUpdateLogin) {
		this.startDate = startDate;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
		this.lastUpdateLogin = lastUpdateLogin;
	}

	public MstRoles(String roleName, String roleDesc, String status, Date startDate, Date endDate, int createdBy,
			Date creationDate, int lastUpdateLogin, Integer lastUpdatedBy, Date lastUpdateDate,
			Set<MstUserRoles> mstUserRoleses, Set<MastRoleModules> mastRoleModuleses) {
		this.roleName = roleName;
		this.roleDesc = roleDesc;
		this.status = status;
		this.startDate = startDate;
		this.endDate = endDate;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
		this.lastUpdateLogin = lastUpdateLogin;
		this.lastUpdatedBy = lastUpdatedBy;
		this.lastUpdateDate = lastUpdateDate;
		this.mstUserRoleses = mstUserRoleses;
		this.mastRoleModuleses = mastRoleModuleses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ROLE_ID", unique = true, nullable = false)
	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Column(name = "ROLE_NAME", length = 100)
	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Column(name = "ROLE_DESC", length = 240)
	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	@Column(name = "STATUS", length = 30)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "START_DATE", nullable = false, length = 10)
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "END_DATE", length = 10)
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "CREATED_BY", nullable = false)
	public int getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE", nullable = false, length = 10)
	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "LAST_UPDATE_LOGIN", nullable = false)
	public int getLastUpdateLogin() {
		return this.lastUpdateLogin;
	}

	public void setLastUpdateLogin(int lastUpdateLogin) {
		this.lastUpdateLogin = lastUpdateLogin;
	}

	@Column(name = "LAST_UPDATED_BY")
	public Integer getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "LAST_UPDATE_DATE", length = 10)
	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
	@Column(name = "is_deleted")
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "mstRoles", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<MstUserRoles> getMstUserRoleses() {
		return this.mstUserRoleses;
	}

	public void setMstUserRoleses(Set<MstUserRoles> mstUserRoleses) {
		this.mstUserRoleses = mstUserRoleses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "mstRoles", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<MastRoleModules> getMastRoleModuleses() {
		return this.mastRoleModuleses;
	}

	public void setMastRoleModuleses(Set<MastRoleModules> mastRoleModuleses) {
		this.mastRoleModuleses = mastRoleModuleses;
	}

}
