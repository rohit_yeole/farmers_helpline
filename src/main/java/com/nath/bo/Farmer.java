package com.nath.bo;
// Generated Apr 8, 2019 9:31:11 PM by Hibernate Tools 5.1.10.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Farmer generated by hbm2java
 */
@Entity
@Table(name = "farmer" )
public class Farmer implements java.io.Serializable {

	private Integer farmerId;
	private Branch branch;
	private Taluka taluka;
	private String name;
	private String mobileNo;
	private String createdBy;
	private Date createdOn;
	private String modifiedBy;
	private Date modifiedOn;
	private Boolean isDeleted;
	private String villageName;
	private Set<FarmerVarientMapping> farmerVarientMappings = new HashSet<FarmerVarientMapping>(0);

	public Farmer() {
	}

	public Farmer(Integer farmerId, Branch branch, Taluka taluka, String name, String mobileNo, String createdBy,
			Date createdOn, String modifiedBy, Date modifiedOn, Boolean isDeleted, String villageName,
			Set<FarmerVarientMapping> farmerVarientMappings) {
		super();
		this.farmerId = farmerId;
		this.branch = branch;
		this.taluka = taluka;
		this.name = name;
		this.mobileNo = mobileNo;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.isDeleted = isDeleted;
		this.villageName = villageName;
		this.farmerVarientMappings = farmerVarientMappings;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "farmer_id", unique = true, nullable = false)
	public Integer getFarmerId() {
		return this.farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch_code", nullable = false)
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "taluka_code", nullable = false)
	public Taluka getTaluka() {
		return this.taluka;
	}

	public void setTaluka(Taluka taluka) {
		this.taluka = taluka;
	}

	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "mobile_no", length = 12)
	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Column(name = "created_by", length = 100)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_on", length = 26)
	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "modified_by", length = 100)
	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_on", length = 26)
	public Date getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name = "is_deleted")
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name = "village_name", length = 100)
	public String getVillageName() {
		return this.villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "farmer", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<FarmerVarientMapping> getFarmerVarientMappings() {
		return this.farmerVarientMappings;
	}

	public void setFarmerVarientMappings(Set<FarmerVarientMapping> farmerVarientMappings) {
		this.farmerVarientMappings = farmerVarientMappings;
	}

}
