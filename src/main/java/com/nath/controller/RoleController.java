/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.dto.RoleAddDTO;
import com.nath.dto.RolesDTO;
import com.nath.dto.RoleUpdateDTO;
import com.nath.service.RoleService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/role")
public class RoleController {

	@Autowired
	RoleService roleService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllCountries(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		List<RolesDTO> countryDTOs = new ArrayList<RolesDTO>();
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		try {
			countryDTOs = roleService.getRoles(page, limit);
			if (null != countryDTOs && countryDTOs.size() != 0) {
				Long count = roleService.getRolesCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(countryDTOs);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getRoleById(@PathVariable("id") int id) {
		ResponseDTO dto = new ResponseDTO();
		RolesDTO roleDTO = new RolesDTO();
		try {
			roleDTO = roleService.getRoleById(id);
			if (null != roleDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(roleDTO);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addRole(@RequestBody RoleAddDTO role) {
		ResponseDTO dto = new ResponseDTO();
		String id;
		try {
			id = roleService.addRole(role);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateRole(@RequestBody RoleUpdateDTO roleUpdateDTO) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (roleService.updateRole(roleUpdateDTO)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + roleUpdateDTO.getRoleName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteRole(@RequestBody int id) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (roleService.deleteRole(id)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

}
