/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.BranchAddDTO;
import com.nath.dto.BranchDTO;
import com.nath.dto.BranchUpdateDTO;
import com.nath.dto.Constants;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.service.BranchService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/branch")
public class BranchController {

	@Autowired
	BranchService branchService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllBranches(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		List<BranchDTO> branchs = new ArrayList<>();

		try {
			branchs = branchService.getBranches(page, limit);
			if (null != branchs && branchs.size() != 0) {
				Long count = branchService.getBranchesCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(branchs);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getBranchById(@PathVariable("code") String code) {
		ResponseDTO dto = new ResponseDTO();
		BranchDTO branchDTO;
		try {
			branchDTO = branchService.getBranchByCode(code);
			if (null != branchDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(branchDTO);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with id : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}

		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addBranch(@RequestBody BranchAddDTO branch) {
		ResponseDTO dto = new ResponseDTO();
		String id;
		try {
			id = branchService.addBranch(branch);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateBranch(@RequestBody BranchUpdateDTO branch) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (branchService.updateBranch(branch)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
//				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with code : " + branch.getBranchCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteBranch(@RequestBody String code) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (branchService.deleteBranch(code.replaceAll("\"", ""))) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
//				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with code : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getBranchesCount() {
		ResponseDTO dto = new ResponseDTO();
		try {
			Long count;
			count = branchService.getBranchesCount();
			if (null != count || count != 0) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(count);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

}
