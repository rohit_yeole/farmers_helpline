/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.dto.StateAddDTO;
import com.nath.dto.StateDTO;
import com.nath.dto.StateUpdateDTO;
import com.nath.service.StateService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/state")
public class StateController {

	@Autowired
	StateService stateService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllStates(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		List<StateDTO> states = new ArrayList<>();

		try {
			states = stateService.getStates(page, limit);
			if (null != states && states.size() != 0) {
				Long count = stateService.getStatesCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(states);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/countryCode/{countryCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getStateByCountryCode(@PathVariable("countryCode") String countryCode) {
		ResponseDTO dto = new ResponseDTO();
		List<StateDTO> states = new ArrayList<>();

		try {
			states = stateService.getStatesByCountryCode(countryCode);
			if (null != states || !states.isEmpty()) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(states);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getStateByCode(@PathVariable("code") String code) {
		ResponseDTO dto = new ResponseDTO();
		StateDTO stateDTO;
		try {
			stateDTO = stateService.getStateByCode(code);
			if (null != stateDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(stateDTO);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with id : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}

		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addState(@RequestBody StateAddDTO state) {
		ResponseDTO dto = new ResponseDTO();
		String id;
		try {
			id = stateService.addState(state);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateState(@RequestBody StateUpdateDTO state) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (stateService.updateState(state)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
//				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with code : " + state.getStateCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteState(@RequestBody String code) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (stateService.deleteState(code.replaceAll("\"", ""))) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
//				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with code : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

}
