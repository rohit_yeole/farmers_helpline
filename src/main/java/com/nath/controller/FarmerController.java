/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.FarmerAddDTO;
import com.nath.dto.FarmerDTO;
import com.nath.dto.FarmerSearchDTO;
import com.nath.dto.FarmerUpdateDTO;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.service.FarmerService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/farmer")
public class FarmerController {

	@Autowired
	FarmerService farmerService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllFarmers(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		List<FarmerDTO> farmers = new ArrayList<>();
		try {
			int offset = (page - 1) * limit;
			farmers = farmerService.getFarmers(offset, limit);
			if (null != farmers && farmers.size() != 0) {
				Long count = farmerService.getFarmersCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(farmers);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getFarmerById(@PathVariable("id") Integer id) {
		ResponseDTO dto = new ResponseDTO();
		FarmerDTO farmerDTO;
		try {
			farmerDTO = farmerService.getFarmerById(id);
			if (null != farmerDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(farmerDTO);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addFarmer(@RequestBody FarmerAddDTO farmerAddDTO) {
		ResponseDTO dto = new ResponseDTO();
		Integer id;
		try {
			id = farmerService.addFarmer(farmerAddDTO);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateFarmer(@RequestBody FarmerUpdateDTO farmerUpdateDTO) {
//		farmerService.updateFarmer(farmer);
		ResponseDTO dto = new ResponseDTO();
		try {
			if (farmerService.updateFarmer(farmerUpdateDTO)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with id : " + farmerUpdateDTO.getFarmerId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteFarmer(@RequestBody Integer id) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (farmerService.deleteFarmer(id)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getAllFarmersByFiler(@RequestBody FarmerSearchDTO farmerSearchDTO) {
		ResponseDTO dto = new ResponseDTO();
		List<FarmerDTO> farmers = new ArrayList<>();
		try {
			farmers = farmerService.getFarmersByFilter(farmerSearchDTO);
			if (null != farmers || !farmers.isEmpty()) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(farmers);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getFarmersCount() {
		ResponseDTO dto = new ResponseDTO();
		try {
			Long count;
			count = farmerService.getFarmersCount();
			if (null != count || count != 0) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(count);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

}
