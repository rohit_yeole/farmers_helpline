/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.CountryAddDTO;
import com.nath.dto.CountryDTO;
import com.nath.dto.CountryUpdateDTO;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.service.CountryService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/country")
public class CountryController {

	@Autowired
	CountryService countryService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllCountries(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		List<CountryDTO> countryDTOs = new ArrayList<CountryDTO>();
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		try {
			countryDTOs = countryService.getCountries(page, limit);
			if (null != countryDTOs && countryDTOs.size() != 0) {
				Long count = countryService.getCountriesCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(countryDTOs);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getCountryById(@PathVariable("code") String code) {
		ResponseDTO dto = new ResponseDTO();
		CountryDTO countryDTO = new CountryDTO();
		try {
			countryDTO = countryService.getCountryById(code);
			if (null != countryDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(countryDTO);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addCountry(@RequestBody CountryAddDTO country1) {
		ResponseDTO dto = new ResponseDTO();
		String id;
		try {
			id = countryService.addCountry(country1);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateCountry(@RequestBody CountryUpdateDTO countryUpdateDTO) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (countryService.updateCountry(countryUpdateDTO)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + countryUpdateDTO.getCountryCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteCountry(@RequestBody String code) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (countryService.deleteCountry(code)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

}
