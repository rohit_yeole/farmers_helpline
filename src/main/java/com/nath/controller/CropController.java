/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.CropAddDTO;
import com.nath.dto.CropDTO;
import com.nath.dto.CropUpdateDTO;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.service.CropService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/crop")
public class CropController {

	@Autowired
	CropService cropService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllCountries(@RequestParam(defaultValue = "	") int page,
			@RequestParam(defaultValue = "10") int limit) {
		List<CropDTO> cropDTOs = new ArrayList<CropDTO>();
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		try {
			cropDTOs = cropService.getCrops(page, limit);
			if (null != cropDTOs && cropDTOs.size() != 0) {
				Long count = cropService.getCropsCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(cropDTOs);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getCropByCode(@PathVariable("code") String code) {
		ResponseDTO dto = new ResponseDTO();
		CropDTO cropDTO = new CropDTO();
		try {
			cropDTO = cropService.getCropByCode(code);
			if (null != cropDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(cropDTO);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addCrop(@RequestBody CropAddDTO crop1) {
		ResponseDTO dto = new ResponseDTO();
		String id;
		try {
			id = cropService.addCrop(crop1);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateCrop(@RequestBody CropUpdateDTO cropUpdateDTO) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (cropService.updateCrop(cropUpdateDTO)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + cropUpdateDTO.getCropCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteCrop(@RequestBody String code) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (cropService.deleteCrop(code)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

}
