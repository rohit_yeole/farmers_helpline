package com.nath.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nath.dao.impl.FileDaoImpl;
import com.nath.dto.Constants;
import com.nath.dto.ResponseDTO;
import com.nath.model.DistrictStgModel;
import com.nath.model.FarmerModel;
import com.nath.model.FileUploadRequest;
import com.nath.model.ReportRequestModel;
import com.nath.model.TalukaStgModel;
import com.nath.model.VarientStgModel;
import com.nath.model.VarientStgResponseModel;
import com.nath.service.FileStorageService;
import com.nath.web.payload.UploadFileResponse;

@RestController
@RequestMapping(value = "/api/file", produces = MediaType.APPLICATION_JSON_VALUE)
public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	FileDaoImpl filedao;

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
//	@ApiImplicitParams({@ApiImplicitParam(name = "access_token", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
	public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
		String fileName = fileStorageService.storeFile(file);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/downloadFile/")
				.path(fileName).toUriString();

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
	}

	@RequestMapping(value = "/uploadFileRead", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseDTO uploadFileRead(@RequestParam("file") MultipartFile file)
			throws IOException, InterruptedException {

		System.out.println(" Hiha ---- ");
		String fileName = fileStorageService.storeFile(file);
		ResponseDTO dto = new ResponseDTO();

		BufferedReader br = null;
		FileReader fr = null;
		int requestID;
		List<FarmerModel> fmList = new ArrayList<FarmerModel>();
		requestID = filedao.insertReuest(new ReportRequestModel(1, fileName, "Inprogress"), "FARMERS");

		System.out.println(" File upload id" + requestID);

		try {

			// fr = new FileReader(f);
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));

			fmList = fileread(is);
			fmList.remove(0);
//            filedao.insertReuest();

			filedao.insert(fmList, requestID);
			fmList.clear();
			fmList = filedao.getFileuploadedData(requestID);
			filedao.insert(fmList);

		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		} finally {
			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				System.err.format("IOException: %s%n", ex);
			}
		}
		filedao.validate(requestID);
		filedao.insertDataintoFarmer(requestID);
		filedao.updateRequestStatus(requestID);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/downloadFile/")
				.path(fileName).toUriString();

		dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
		dto.setObject(requestID);
		dto.setMessage(" File uploaded successfully");
		// return fmList;
		return dto;
	}

	@RequestMapping(value = "/uploadFileDistrict", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseDTO uploadFileDistrict(@RequestParam("file") MultipartFile file)
			throws IOException, InterruptedException {

		System.out.println(" Hiha ---- ");
		String fileName = fileStorageService.storeFile(file);
		ResponseDTO dto = new ResponseDTO();

		BufferedReader br = null;
		FileReader fr = null;
		int requestID;
		List<DistrictStgModel> distList = new ArrayList<DistrictStgModel>();
		requestID = filedao.insertReuest(new ReportRequestModel(1, fileName, "Inprogress"), "DISTRICT");

		System.out.println(" File upload id" + requestID);

		try {

			// fr = new FileReader(f);
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));

			distList = filereadDistrict(is);
			distList.remove(0);
//          filedao.insertReuest();

			filedao.insertDistrictStg(distList, requestID);
			distList.clear();
			// distList=filedao.getFileuploadedData(requestID);
			filedao.insertDataDistrict(requestID);

		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		} finally {
			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				System.err.format("IOException: %s%n", ex);
			}
		}
		// filedao.validate(requestID);
		// filedao.insertDataintoFarmer(requestID);
		// filedao.updateRequestStatus(requestID);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/downloadFile/")
				.path(fileName).toUriString();

		dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
		dto.setObject(requestID);
		dto.setMessage(" File uploaded successfully");
		// return fmList;
		return dto;
	}

	@RequestMapping(value = "/uploadFileTaluka", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseDTO uploadFileTaluka(@RequestParam("file") MultipartFile file)
			throws IOException, InterruptedException {

		System.out.println(" Hiha ---- ");
		String fileName = fileStorageService.storeFile(file);
		ResponseDTO dto = new ResponseDTO();

		BufferedReader br = null;
		FileReader fr = null;
		int requestID;
		List<TalukaStgModel> talukaList = new ArrayList<TalukaStgModel>();
		requestID = filedao.insertReuest(new ReportRequestModel(1, fileName, "Inprogress"), "TALUKA");

		System.out.println(" File upload id" + requestID);

		try {

			// fr = new FileReader(f);
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));

			talukaList = filereadTaluka(is);
			talukaList.remove(0);
//          filedao.insertReuest();

			filedao.insertTalukaStg(talukaList, requestID);
			talukaList.clear();
			// distList=filedao.getFileuploadedData(requestID);
			filedao.insertDataTaluka(requestID);

		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		} finally {
			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				System.err.format("IOException: %s%n", ex);
			}
		}
		// filedao.validate(requestID);
		// filedao.insertDataintoFarmer(requestID);
		// filedao.updateRequestStatus(requestID);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/downloadFile/")
				.path(fileName).toUriString();

		dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
		dto.setObject(requestID);
		dto.setMessage(" File uploaded successfully");
		// return fmList;
		return dto;
	}

	@RequestMapping(value = "/uploadFileCrop", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseDTO uploadFileVarient(@RequestParam("file") MultipartFile file)
			throws IOException, InterruptedException {

		System.out.println(" File Varient Mapping ---- ");
		String fileName = fileStorageService.storeFile(file);
		ResponseDTO dto = new ResponseDTO();

		BufferedReader br = null;
		FileReader fr = null;
		int requestID;
		List<VarientStgModel> varientList = new ArrayList<VarientStgModel>();
		List<VarientStgResponseModel> varientResponseList = new ArrayList<VarientStgResponseModel>();
		requestID = filedao.insertReuest(new ReportRequestModel(1, fileName, "Inprogress"), "CROP");

		System.out.println("File upload id : " + requestID);

		try {
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));

			varientList = filereadVarient(is);

			filedao.insertVarientStg(varientList, requestID);

			varientList = filedao.getVarientListFromStg(requestID);
			varientResponseList = filedao.insertDataVarient(varientList);
			filedao.updateVarientListInStg(varientResponseList);

		} catch (IOException e) {
			System.err.format("IOException: %s%n ", e);
		} finally {
			try {
				if (br != null)
					br.close();

				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				System.err.format("IOException: %s%n", ex);
			}
		}

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/downloadFile/")
				.path(fileName).toUriString();

		dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
		dto.setObject(requestID);
		dto.setMessage(" File uploaded successfully");
		// return fmList;

		ByteArrayInputStream in = customersToExcel(varientResponseList);
		// return IOUtils.toByteArray(in);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=customers.xlsx");

//		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
		return dto;
	}

	@RequestMapping(value = "/uploadMultipleFiles", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	// @ApiImplicitParams({@ApiImplicitParam(name = "access_token", value =
	// "Authorization token", required = true, dataType = "string", paramType =
	// "header") })
	public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
		return Arrays.asList(files).stream().map(file -> uploadFile(file)).collect(Collectors.toList());
	}

	@RequestMapping(value = "/downloadFile/{fileName:.+}", method = RequestMethod.GET)
	// @ApiImplicitParams({@ApiImplicitParam(name = "access_token", value =
	// "Authorization token", required = true, dataType = "string", paramType =
	// "header") })
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		Resource resource = fileStorageService.loadFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	public static ByteArrayInputStream customersToExcel(List<VarientStgResponseModel> list) throws IOException {
		String[] COLUMNs = { "MobileNo", "CropCode", "VarientCode", "Comment" };
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Customers");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			// Row for Header
			Row headerRow = sheet.createRow(0);

			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}

			int rowIdx = 1;
			for (VarientStgResponseModel customer : list) {
				Row row = sheet.createRow(rowIdx++);

				row.createCell(0).setCellValue(customer.getMobileNo());
				row.createCell(1).setCellValue(customer.getCropCode());
				row.createCell(2).setCellValue(customer.getVarientCode());
				row.createCell(3).setCellValue(customer.getComment());
			}

			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}

	public List fileread(InputStream f) throws EncryptedDocumentException, IOException {
		// Creating a Workbook from an Excel file (.xls or .xlsx)
		Workbook workbook = WorkbookFactory.create(f);

		// Retrieving the number of sheets in the Workbook
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

		/*
		 * ============================================================= Iterating over
		 * all the sheets in the workbook (Multiple ways)
		 * =============================================================
		 */

		// 1. You can obtain a sheetIterator and iterate over it
		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		System.out.println("Retrieving Sheets using Iterator");
		while (sheetIterator.hasNext()) {
			Sheet sheet = sheetIterator.next();
			System.out.println("=> " + sheet.getSheetName());
		}

		// 2. Or you can use a for-each loop
		System.out.println("Retrieving Sheets using for-each loop");
		for (Sheet sheet : workbook) {
			System.out.println("=> " + sheet.getSheetName());
		}

		// 3. Or you can use a Java 8 forEach with lambda
		System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
		workbook.forEach(sheet -> {
			System.out.println("=> " + sheet.getSheetName());
		});

		/*
		 * ================================================================== Iterating
		 * over all the rows and columns in a Sheet (Multiple ways)
		 * ==================================================================
		 */

		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		// 1. You can obtain a rowIterator and columnIterator and iterate over them
		System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		Iterator<Row> rowIterator = sheet.rowIterator();
		ArrayList<FarmerModel> fmList = new ArrayList<FarmerModel>();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			FarmerModel fm = new FarmerModel();
			// Now let's iterate over the columns of the current row
			Iterator<Cell> cellIterator = row.cellIterator();
			int i = 0;

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
				i++;
				if (i == 1) {
					fm.setNo((cellValue));
				}
				if (i == 2) {
					fm.setMobileNo(cellValue);
				}
				if (i == 3) {
					fm.setFarmerName(cellValue);
				}
				if (i == 4) {
					fm.setVillage(cellValue);
				}
				if (i == 5) {
					fm.setTaluka(cellValue);
				}
				if (i == 6) {
					fm.setDistrict(cellValue);
				}
				if (i == 7) {
					fm.setState(cellValue);
				}
				if (i == 8) {
					fm.setWinBranch(cellValue);
				}
				if (i == 9) {
					fm.setBranch(cellValue);
				}
				if (i == 10) {
					fm.setDigit(cellValue);
				}

			}
			if (fm.getMobileNo().length() > 0)
				fmList.add(fm);
			System.out.println();
		}

		// 2. Or you can use a for-each loop to iterate over the rows and columns
		System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
		for (Row row : sheet) {
			for (Cell cell : row) {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			}
		}

		// 3. Or you can use Java 8 forEach loop with lambda

		System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
		sheet.forEach(row -> {
			int rowcount = 0;

			row.forEach(cell -> {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			});

			System.out.println();
			// System.out.println("rowcount "+rowcount);
			rowcount++;

		});

		// Closing the workbook
		workbook.close();
		System.out.println(" fmList " + fmList.toString());

		return fmList;
	}

	public List filereadDistrict(InputStream f) throws EncryptedDocumentException, IOException {
		// Creating a Workbook from an Excel file (.xls or .xlsx)
		Workbook workbook = WorkbookFactory.create(f);

		// Retrieving the number of sheets in the Workbook
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

		/*
		 * ============================================================= Iterating over
		 * all the sheets in the workbook (Multiple ways)
		 * =============================================================
		 */

		// 1. You can obtain a sheetIterator and iterate over it
		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		System.out.println("Retrieving Sheets using Iterator");
		while (sheetIterator.hasNext()) {
			Sheet sheet = sheetIterator.next();
			System.out.println("=> " + sheet.getSheetName());
		}

		// 2. Or you can use a for-each loop
		System.out.println("Retrieving Sheets using for-each loop");
		for (Sheet sheet : workbook) {
			System.out.println("=> " + sheet.getSheetName());
		}

		// 3. Or you can use a Java 8 forEach with lambda
		System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
		workbook.forEach(sheet -> {
			System.out.println("=> " + sheet.getSheetName());
		});

		/*
		 * ================================================================== Iterating
		 * over all the rows and columns in a Sheet (Multiple ways)
		 * ==================================================================
		 */

		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		// 1. You can obtain a rowIterator and columnIterator and iterate over them
		System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		Iterator<Row> rowIterator = sheet.rowIterator();
		ArrayList<DistrictStgModel> distList = new ArrayList<DistrictStgModel>();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			DistrictStgModel fm = new DistrictStgModel();
			// Now let's iterate over the columns of the current row
			Iterator<Cell> cellIterator = row.cellIterator();
			int i = 0;

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
				i++;
				if (i == 1) {
					fm.setDistName((cellValue));
				}
				if (i == 2) {
					fm.setDistCode(cellValue);
				}
				if (i == 3) {
					fm.setStateName(cellValue);
				}
				if (i == 4) {
					fm.setStateCode(cellValue);
				}

			}
			distList.add(fm);
			System.out.println();
		}

		// 2. Or you can use a for-each loop to iterate over the rows and columns
		System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
		for (Row row : sheet) {
			for (Cell cell : row) {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			}
		}

		// 3. Or you can use Java 8 forEach loop with lambda

		System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
		sheet.forEach(row -> {
			int rowcount = 0;

			row.forEach(cell -> {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			});

			System.out.println();
			// System.out.println("rowcount "+rowcount);
			rowcount++;

		});

		// Closing the workbook
		workbook.close();
		System.out.println(" fmList " + distList.toString());

		return distList;
	}

	public List filereadTaluka(InputStream f) throws EncryptedDocumentException, IOException {
		// Creating a Workbook from an Excel file (.xls or .xlsx)
		Workbook workbook = WorkbookFactory.create(f);

		// Retrieving the number of sheets in the Workbook
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

		/*
		 * ============================================================= Iterating over
		 * all the sheets in the workbook (Multiple ways)
		 * =============================================================
		 */

		// 1. You can obtain a sheetIterator and iterate over it
		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		System.out.println("Retrieving Sheets using Iterator");
		while (sheetIterator.hasNext()) {
			Sheet sheet = sheetIterator.next();
			System.out.println("=> " + sheet.getSheetName());
		}

		// 2. Or you can use a for-each loop
		System.out.println("Retrieving Sheets using for-each loop");
		for (Sheet sheet : workbook) {
			System.out.println("=> " + sheet.getSheetName());
		}

		// 3. Or you can use a Java 8 forEach with lambda
		System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
		workbook.forEach(sheet -> {
			System.out.println("=> " + sheet.getSheetName());
		});

		/*
		 * ================================================================== Iterating
		 * over all the rows and columns in a Sheet (Multiple ways)
		 * ==================================================================
		 */

		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		// 1. You can obtain a rowIterator and columnIterator and iterate over them
		System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		Iterator<Row> rowIterator = sheet.rowIterator();
		ArrayList<TalukaStgModel> talukaList = new ArrayList<TalukaStgModel>();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			TalukaStgModel fm = new TalukaStgModel();
			// Now let's iterate over the columns of the current row
			Iterator<Cell> cellIterator = row.cellIterator();
			int i = 0;

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
				i++;
				if (i == 1) {
					fm.setTalukaName((cellValue));
				}
				if (i == 2) {
					fm.setTalukaCode(cellValue);
				}
				if (i == 3) {
					fm.setDistrictName(cellValue);
				}
				if (i == 4) {
					fm.setDistrictCode(cellValue);
				}

			}
			talukaList.add(fm);
			System.out.println();
		}

		// 2. Or you can use a for-each loop to iterate over the rows and columns
		System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
		for (Row row : sheet) {
			for (Cell cell : row) {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			}
		}

		// 3. Or you can use Java 8 forEach loop with lambda

		System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
		sheet.forEach(row -> {
			int rowcount = 0;

			row.forEach(cell -> {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			});

			System.out.println();
			// System.out.println("rowcount "+rowcount);
			rowcount++;

		});

		// Closing the workbook
		workbook.close();
		System.out.println(" fmList " + talukaList.toString());

		return talukaList;
	}

	public List filereadVarient(InputStream f) throws EncryptedDocumentException, IOException {
		// Creating a Workbook from an Excel file (.xls or .xlsx)
		Workbook workbook = WorkbookFactory.create(f);

		// Retrieving the number of sheets in the Workbook
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

		/*
		 * ============================================================= Iterating over
		 * all the sheets in the workbook (Multiple ways)
		 * =============================================================
		 */

		// 1. You can obtain a sheetIterator and iterate over it
		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		System.out.println("Retrieving Sheets using Iterator");
		while (sheetIterator.hasNext()) {
			Sheet sheet = sheetIterator.next();
			System.out.println("=> " + sheet.getSheetName());
		}

		// 2. Or you can use a for-each loop
		System.out.println("Retrieving Sheets using for-each loop");
		for (Sheet sheet : workbook) {
			System.out.println("=> " + sheet.getSheetName());
		}

		// 3. Or you can use a Java 8 forEach with lambda
		System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
		workbook.forEach(sheet -> {
			System.out.println("=> " + sheet.getSheetName());
		});

		/*
		 * ================================================================== Iterating
		 * over all the rows and columns in a Sheet (Multiple ways)
		 * ==================================================================
		 */

		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		// 1. You can obtain a rowIterator and columnIterator and iterate over them
		System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		Iterator<Row> rowIterator = sheet.rowIterator();
		ArrayList<VarientStgModel> varientList = new ArrayList<VarientStgModel>();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			VarientStgModel vm = new VarientStgModel();
			// Now let's iterate over the columns of the current row
			Iterator<Cell> cellIterator = row.cellIterator();
			int i = 0;

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
				i++;
				if (i == 1) {
					vm.setMobileNo(cellValue);
				}
				if (i == 2) {
					vm.setCropCode(cellValue);
				}
				if (i == 3) {
					vm.setVarientCode(cellValue);
				}

			}
			varientList.add(vm);
			System.out.println();
		}

		// 2. Or you can use a for-each loop to iterate over the rows and columns
		System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
		for (Row row : sheet) {
			for (Cell cell : row) {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			}
		}

		// 3. Or you can use Java 8 forEach loop with lambda

		System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
		sheet.forEach(row -> {
			int rowcount = 0;

			row.forEach(cell -> {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");

			});

			System.out.println();
			// System.out.println("rowcount "+rowcount);
			rowcount++;

		});

		// Closing the workbook
		workbook.close();
		System.out.println(" fmList " + varientList.toString());

		return varientList;
	}

	@RequestMapping(value = "/fileUploadRequestdatatable/filetype/{filetype}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	// @ApiImplicitParams({@ApiImplicitParam(name = "access_token", value =
	// "Authorization token", required = true, dataType = "string", paramType =
	// "header") })
	public ResponseDTO fileUploadRequest(@PathVariable("filetype") String fileType) {
		ResponseDTO dto = new ResponseDTO();
		List<FileUploadRequest> fileUploadList = new ArrayList<FileUploadRequest>();

		fileUploadList = filedao.getListoffileuplod(fileType);
		dto.setObject(fileUploadList);
		dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
		return dto;
	}

	@RequestMapping(value = "/farmerFileRequestbyRequestId/{requestID}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseDTO FarmerFileRequestbyRequestId(@PathVariable("requestID") int requestID) {

		ResponseDTO dto = new ResponseDTO();

		List<FarmerModel> fmList = new ArrayList<FarmerModel>();

		fmList = filedao.getFileuploadedData(requestID);

		dto.setObject(fmList);
		dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);

		return dto;
	}
}