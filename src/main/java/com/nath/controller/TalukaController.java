/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.dto.TalukaAddDTO;
import com.nath.dto.TalukaDTO;
import com.nath.dto.TalukaUpdateDTO;
import com.nath.service.TalukaService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/taluka")
public class TalukaController {

	@Autowired
	TalukaService talukaService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllTalukas(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		List<TalukaDTO> talukas = new ArrayList<>();
		try {
			talukas = talukaService.getTalukas(page, limit);
			if (null != talukas && talukas.size() != 0) {
				Long count = talukaService.getTalukasCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(talukas);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/districtCode/{districtCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getAllTalukasByDistrictCode(@PathVariable("districtCode") String districtCode) {
		ResponseDTO dto = new ResponseDTO();
		List<TalukaDTO> talukas = new ArrayList<>();
		try {
			talukas = talukaService.getTalukasByDistrictCode(districtCode);
			if (null != talukas || !talukas.isEmpty()) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(talukas);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getTalukaById(@PathVariable("code") String code) {
//		return talukaService.getTalukaById(id);
		ResponseDTO dto = new ResponseDTO();
		TalukaDTO talukaDTO;
		try {
			talukaDTO = talukaService.getTalukaByCode(code);
			if (null != talukaDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(talukaDTO);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with code : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_COUNTRY_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addTaluka(@RequestBody TalukaAddDTO taluka) {
		ResponseDTO dto = new ResponseDTO();
		String id;
		try {
			id = talukaService.addTaluka(taluka);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateTaluka(@RequestBody TalukaUpdateDTO taluka) {
//		talukaService.updateTaluka(taluka);
		ResponseDTO dto = new ResponseDTO();
		try {
			if (talukaService.updateTaluka(taluka)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with code : " + taluka.getTalukaCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteTaluka(@RequestBody String code) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (talukaService.deleteTaluka(code.replaceAll("\"", ""))) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists with code : " + code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

}
