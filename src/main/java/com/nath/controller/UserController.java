/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.dto.UserAddDTO;
import com.nath.dto.UserDTO;
import com.nath.dto.UserUpdateDTO;
import com.nath.dto.UserValidateDTO;
import com.nath.dto.UserValidateResponseDTO;
import com.nath.service.UserService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO loginUser(@RequestBody UserValidateDTO validateDTO) {
		ResponseDTO dto = new ResponseDTO();
		UserValidateResponseDTO validateResponseDTO;

		try {
			validateResponseDTO = userService.authenticateUser(validateDTO);
			if (null != validateResponseDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(validateResponseDTO);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("Please check your Username and Password ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllCountries(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		List<UserDTO> countryDTOs = new ArrayList<UserDTO>();
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		try {
			countryDTOs = userService.getUsers(page, limit);
			if (null != countryDTOs && countryDTOs.size() != 0) {
				Long count = userService.getUsersCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(countryDTOs);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getUserById(@PathVariable("id") int id) {
		ResponseDTO dto = new ResponseDTO();
		UserDTO userDTO = new UserDTO();
		try {
			userDTO = userService.getUserById(id);
			if (null != userDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(userDTO);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addCountry(@RequestBody UserAddDTO user) {
		ResponseDTO dto = new ResponseDTO();
		Integer id;
		try {
			id = userService.addUser(user);
			if (id.equals(null)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("User with name " + user.getUserName() + " already exists !");
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_USER_SUCCESS);
			}

		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateCountry(@RequestBody UserUpdateDTO userUpdateDTO) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (userService.updateUser(userUpdateDTO)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + userUpdateDTO.getUserName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteCountry(@RequestBody int id) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (userService.deleteUser(id)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/validate/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Boolean validateUsername(@PathVariable("username") String username) {
		Boolean isValid = true;
		try {
			isValid = userService.validateUsername(username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isValid;
	}
}
