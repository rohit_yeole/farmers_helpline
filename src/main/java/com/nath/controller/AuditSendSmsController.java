package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nath.dao.impl.AuditSendSmsDaoImpl;
import com.nath.dto.Constants;
import com.nath.dto.ResponseDTO;
import com.nath.model.AuditSendSms;

@RestController
@RequestMapping(value = "/api/auditsms",produces = MediaType.APPLICATION_JSON_VALUE)
@PropertySource("classpath:database.properties")
public class AuditSendSmsController {
	
	@Autowired
	AuditSendSmsDaoImpl auditsms =new AuditSendSmsDaoImpl();
	
	@Autowired
	private Environment env;
	
    @RequestMapping(value="/getallaudit",method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseDTO getAllAudit() {
		
		ResponseDTO dto = new ResponseDTO();
		List<AuditSendSms> auditList=new ArrayList<AuditSendSms>();

		auditList=auditsms.getAuditSendSmsData();
		
		
		
		if (null != auditList || !auditList.isEmpty()) {
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setObject(auditList);
		} else {
			dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
			dto.setMessage("No Record Exists !");
		}
		return dto;
	}
    
    
    @RequestMapping(value="/insert",method = RequestMethod.POST,produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseDTO getInsertAudit(@RequestBody AuditSendSms ass ) {
    	
		ResponseDTO dto = new ResponseDTO();
		auditsms.insert(ass);
		sendSMSUnicode(ass);
		dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
		return dto;

    }

    public void sendSMSUnicode(AuditSendSms ass) {
    	
    	//		dataSource.setDriverClassName(env.getProperty("database.driver"));

    	
    	RestTemplate restTemplate = new RestTemplate();
    	String url=env.getProperty("appurl.url")+ass.getMobileNumber()+env.getProperty("appurl.from")+ass.getSms()+env.getProperty("appurl.code");
    	
    	System.out.println( " URL "+url );
    	restTemplate.getForObject(url,String.class);
    	
    	
    }
    
    public void sendSMS	() {
    	
    	
    }
    
}
