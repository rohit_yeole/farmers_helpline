/**
 * 
 */
package com.nath.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nath.dto.Constants;
import com.nath.dto.ModuleAddDTO;
import com.nath.dto.ModuleUpdateDTO;
import com.nath.dto.ModulesDTO;
import com.nath.dto.ResponseDTO;
import com.nath.dto.ResponsePaginationDTO;
import com.nath.service.ModuleService;

/**
 * @author Rohit
 *
 */
@RestController
@RequestMapping(value = "/module")
public class ModuleController {

	@Autowired
	ModuleService moduleService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponsePaginationDTO getAllCountries(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		List<ModulesDTO> countryDTOs = new ArrayList<ModulesDTO>();
		ResponsePaginationDTO dto = new ResponsePaginationDTO();
		try {
			countryDTOs = moduleService.getModules(page, limit);
			if (null != countryDTOs && countryDTOs.size() != 0) {
				Long count = moduleService.getModulesCount();
				int totalPages = (int) Math.ceil((double) count / limit);
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setCurrentPage(page);
				dto.setPageCount(totalPages == 0 && count > 0 ? 1 : totalPages);
				dto.setObject(countryDTOs);
			} else {
				dto.setMessageType(Constants.MESSAGE_TYPE_WARNING);
				dto.setMessage("No Record Exists !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO getCountryById(@PathVariable("id") int id) {
		ResponseDTO dto = new ResponseDTO();
		ModulesDTO moduleDTO = new ModulesDTO();
		try {
			moduleDTO = moduleService.getModuleById(id);
			if (null != moduleDTO) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setObject(moduleDTO);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO addCountry(@RequestBody ModuleAddDTO module) {
		ResponseDTO dto = new ResponseDTO();
		String id;
		try {
			id = moduleService.addModule(module);
			dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			dto.setMessage(Constants.MESSAGE_USER_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			Throwable throwable = new Throwable(e.getCause());
			dto.setMessage(throwable.getLocalizedMessage());
		}
		return dto;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO updateCountry(@RequestBody ModuleUpdateDTO moduleUpdateDTO) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (moduleService.updateModule(moduleUpdateDTO)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
				dto.setMessage(Constants.MESSAGE_COUNTRY_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + moduleUpdateDTO.getModuleName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO deleteModule(@RequestBody int id) {
		ResponseDTO dto = new ResponseDTO();
		try {
			if (moduleService.deleteModule(id)) {
				dto.setMessageType(Constants.MESSAGE_TYPE_SUCCESS);
			} else {
				dto.setMessageType("WARNING");
				dto.setMessage("No Record Exists with id : " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dto.setMessageType(Constants.MESSAGE_TYPE_ERROR);
			dto.setMessage(Constants.MESSAGE_COUNTRY_ERROR);
		}
		return dto;
	}

}
