package com.nath.dto;
// Generated Mar 29, 2019 9:15:29 AM by Hibernate Tools 5.1.10.Final

/**
 * State generated by hbm2java
 */
/**
 * @author Rohit
 *
 */
public class CountryUpdateDTO implements java.io.Serializable {

//	private Integer countryId;
	private String countryCode;
	private String countryName;
	private String modifiedBy;

	public CountryUpdateDTO() {
	}

	public CountryUpdateDTO(Integer countryId, String countryCode, String countryName, String modifiedBy) {
		super();
//		this.countryId = countryId;
		this.countryCode = countryCode;
		this.countryName = countryName;
		this.modifiedBy = modifiedBy;
	}

//	public Integer getCountryId() {
//		return countryId;
//	}
//
//	public void setCountryId(Integer countryId) {
//		this.countryId = countryId;
//	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
