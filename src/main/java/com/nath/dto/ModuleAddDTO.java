package com.nath.dto;
// Generated Mar 29, 2019 9:15:29 AM by Hibernate Tools 5.1.10.Final

/**
 * State generated by hbm2java
 */
/**
 * @author Rohit
 *
 */
public class ModuleAddDTO implements java.io.Serializable {

	private String moduleName;
	private String moduleDesc;
	private String moduleUrl;
	private String moduleGroup;
	private String status;

	public ModuleAddDTO() {
	}

	public ModuleAddDTO(String moduleName, String moduleDesc, String moduleUrl, String moduleGroup, String status) {
		super();
		this.moduleName = moduleName;
		this.moduleDesc = moduleDesc;
		this.moduleUrl = moduleUrl;
		this.moduleGroup = moduleGroup;
		this.status = status;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleDesc() {
		return moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getModuleGroup() {
		return moduleGroup;
	}

	public void setModuleGroup(String moduleGroup) {
		this.moduleGroup = moduleGroup;
	}

}
