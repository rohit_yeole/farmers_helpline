package com.nath.dto;

import java.io.Serializable;
import java.util.Set;

public class RolesDTO implements Serializable {

	private Integer roleId;
	private String roleName;
	private String roleDesc;
	private String status;
	private Set<ModulesDTO> modulesDTO;

	public RolesDTO() {
		super();
	}

	public RolesDTO(Integer roleId, String roleName, String roleDesc, String status, Set<ModulesDTO> modulesDTO) {
		super();
		this.roleId = roleId;
		this.roleName = roleName;
		this.roleDesc = roleDesc;
		this.status = status;
		this.modulesDTO = modulesDTO;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public Set<ModulesDTO> getModulesDTO() {
		return modulesDTO;
	}

	public void setModulesDTO(Set<ModulesDTO> modulesDTO) {
		this.modulesDTO = modulesDTO;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
