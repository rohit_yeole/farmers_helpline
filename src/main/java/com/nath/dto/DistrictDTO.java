package com.nath.dto;
// Generated Mar 29, 2019 9:15:29 AM by Hibernate Tools 5.1.10.Final

/**
 * District generated by hbm2java
 */
public class DistrictDTO implements java.io.Serializable {

//	private Integer districtId;
	private String districtName;
	private String districtCode;
	private String stateCode;
	private String stateName;

	public DistrictDTO() {
	}

	public DistrictDTO(String districtName, String districtCode, String stateCode, String stateName) {
		super();
		this.districtName = districtName;
		this.districtCode = districtCode;
		this.stateCode = stateCode;
		this.stateName = stateName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}
