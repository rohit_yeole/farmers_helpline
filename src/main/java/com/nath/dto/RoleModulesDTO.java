package com.nath.dto;

import java.io.Serializable;

public class RoleModulesDTO implements Serializable {

	private int roleId;
	private int moduleId;
	private String status;

	public RoleModulesDTO() {
		super();
	}

	public RoleModulesDTO(int roleId, int moduleId, String status) {
		super();
		this.roleId = roleId;
		this.moduleId = moduleId;
		this.status = status;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
