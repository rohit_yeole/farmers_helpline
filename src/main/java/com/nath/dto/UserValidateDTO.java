package com.nath.dto;

import java.io.Serializable;

public class UserValidateDTO implements Serializable {

	private String username;
	private String password;

	public UserValidateDTO() {
		super();
	}

	public UserValidateDTO(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
