package com.nath.dto;

import java.io.Serializable;
import java.util.Set;

public class UserValidateResponseDTO implements Serializable {

	private String username;
	private String email;
	private String userType;
	private Set<RolesDTO> rolesDTO;

	public UserValidateResponseDTO() {
		super();
	}

	public UserValidateResponseDTO(String username, String email, String userType, Set<RolesDTO> rolesDTO) {
		super();
		this.username = username;
		this.email = email;
		this.userType = userType;
		this.rolesDTO = rolesDTO;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Set<RolesDTO> getRolesDTO() {
		return rolesDTO;
	}

	public void setRolesDTO(Set<RolesDTO> rolesDTO) {
		this.rolesDTO = rolesDTO;
	}

}
