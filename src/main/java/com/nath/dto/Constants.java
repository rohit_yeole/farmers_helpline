package com.nath.dto;

public class Constants {

	public static final String MESSAGE_TYPE_SUCCESS = "SUCCESS";

	public static final String MESSAGE_TYPE_ERROR = "ERROR";

	public static final String MESSAGE_TYPE_INFO = "INFO";

	public static final String MESSAGE_COUNTRY_ERROR = "Alert ! Error while fetching Country List";

	public static final String MESSAGE_COUNTRY_SUCCESS = "Success ! New Country Added Successfully !";
	
	public static final String MESSAGE_USER_SUCCESS = "Success ! New User Added Successfully !";

	public static final String MESSAGE_TYPE_WARNING = "WARNING";

}
