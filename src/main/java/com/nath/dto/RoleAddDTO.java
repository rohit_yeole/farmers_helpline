package com.nath.dto;

import java.util.Set;

// Generated Mar 29, 2019 9:15:29 AM by Hibernate Tools 5.1.10.Final

/**
 * State generated by hbm2java
 */
/**
 * @author Rohit
 *
 */
public class RoleAddDTO implements java.io.Serializable {

	private String roleName;
	private String roleDesc;
	private String status;
	private Set<Integer> moduleDTOs;

	public RoleAddDTO() {
	}

	public RoleAddDTO(String roleName, String roleDesc, String status, Set<Integer> moduleDTOs) {
		super();
		this.roleName = roleName;
		this.roleDesc = roleDesc;
		this.status = status;
		this.moduleDTOs = moduleDTOs;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Integer> getModuleDTOs() {
		return moduleDTOs;
	}

	public void setModuleDTOs(Set<Integer> moduleDTOs) {
		this.moduleDTOs = moduleDTOs;
	}

}
