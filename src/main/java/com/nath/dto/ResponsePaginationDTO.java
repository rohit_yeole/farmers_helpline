package com.nath.dto;

import java.io.Serializable;

public class ResponsePaginationDTO implements Serializable {

	private String messageType;
	private String message;
	private Object object;
	private int currentPage;
	private int pageCount;

	public ResponsePaginationDTO() {
		super();
	}

	public ResponsePaginationDTO(String messageType, String message, Object object, int currentPage, int pageCount) {
		super();
		this.messageType = messageType;
		this.message = message;
		this.object = object;
		this.currentPage = currentPage;
		this.pageCount = pageCount;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

}
