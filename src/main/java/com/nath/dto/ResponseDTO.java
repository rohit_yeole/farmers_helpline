package com.nath.dto;

import java.io.Serializable;

public class ResponseDTO implements Serializable {

	private String messageType;
	private String message;
	private Object object;

	public ResponseDTO() {
		super();
	}

	public ResponseDTO(String messageType, String message, Object object) {
		super();
		this.messageType = messageType;
		this.message = message;
		this.object = object;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

}
