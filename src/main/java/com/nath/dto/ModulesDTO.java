package com.nath.dto;

import java.io.Serializable;

public class ModulesDTO implements Serializable {

	private int moduleId;
	private String moduleName;
	private String moduleDesc;
	private String moduleUrl;
	private String moduleGroup;
	private String status;

	public ModulesDTO() {
		super();
	}

	public ModulesDTO(int moduleId, String moduleName, String moduleDesc, String moduleUrl, String moduleGroup,
			String status) {
		super();
		this.moduleId = moduleId;
		this.moduleName = moduleName;
		this.moduleDesc = moduleDesc;
		this.moduleUrl = moduleUrl;
		this.moduleGroup = moduleGroup;
		this.status = status;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleDesc() {
		return moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getModuleGroup() {
		return moduleGroup;
	}

	public void setModuleGroup(String moduleGroup) {
		this.moduleGroup = moduleGroup;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
