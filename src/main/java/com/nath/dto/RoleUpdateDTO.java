package com.nath.dto;
// Generated Mar 29, 2019 9:15:29 AM by Hibernate Tools 5.1.10.Final

import java.util.Set;

/**
 * State generated by hbm2java
 */
/**
 * @author Rohit
 *
 */
public class RoleUpdateDTO implements java.io.Serializable {

	private int roleId;
	private String roleName;
	private String roleDesc;
	private String status;
	private Set<Integer> moduleDTOs;

	public RoleUpdateDTO() {
	}

	public RoleUpdateDTO(int roleId, String roleName, String roleDesc, String status, Set<Integer> moduleDTOs) {
		super();
		this.roleId = roleId;
		this.roleName = roleName;
		this.roleDesc = roleDesc;
		this.status = status;
		this.moduleDTOs = moduleDTOs;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Integer> getModuleDTOs() {
		return moduleDTOs;
	}

	public void setModuleDTOs(Set<Integer> moduleDTOs) {
		this.moduleDTOs = moduleDTOs;
	}

}
