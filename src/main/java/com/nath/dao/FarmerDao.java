/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.Farmer;
import com.nath.dto.FarmerSearchDTO;

/**
 * @author Rohit
 *
 */
public interface FarmerDao {

	public List<Farmer> getFarmers(int page, int limit) throws Exception;

	public Farmer getFarmerById(Integer id) throws Exception;

	public Integer addFarmer(Farmer farmer) throws Exception;

	public void updateFarmer(Farmer farmer) throws Exception;

	public List<Farmer> getFarmersByFilter(FarmerSearchDTO farmerSearchDTO) throws Exception;

	public Long getFarmersCount() throws Exception;

	public Farmer getFarmerByMobile(String mobile) throws Exception;

}
