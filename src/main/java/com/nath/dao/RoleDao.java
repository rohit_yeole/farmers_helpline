/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.MstRoles;

/**
 * @author Rohit
 *
 */
public interface RoleDao {

	public List<MstRoles> getRoles(int page, int limit) throws Exception;

	public MstRoles getRolesById(int id) throws Exception;

	public String addRoles(MstRoles roles) throws Exception;

	public void updateRoles(MstRoles roles) throws Exception;

	public Long getRolesCount() throws Exception;
}
