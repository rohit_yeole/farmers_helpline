/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.Branch;

/**
 * @author Rohit
 *
 */
public interface BranchDao {

	public List<Branch> getBranches(int page, int limit) throws Exception;

	public Branch getBranchByCode(String code) throws Exception;

	public String addBranch(Branch branch) throws Exception;

	public void updateBranch(Branch branch) throws Exception;
	
	public Long getBranchesCount() throws Exception;

}
