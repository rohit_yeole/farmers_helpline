/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.Country;

/**
 * @author Rohit
 *
 */
public interface CountryDao {

	public List<Country> getCountries(int page, int limit) throws Exception;

	public Country getCountryById(String code) throws Exception;

	public String addCountry(Country country) throws Exception;

	public void updateCountry(Country country) throws Exception;

	public Long getCountriesCount() throws Exception;

}
