/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.State;

/**
 * @author Rohit
 *
 */
public interface StateDao {

	public List<State> getStates(int page, int limit) throws Exception;

	public List<State> getStatesByCountryCode(String countryCode) throws Exception;

	public State getStateByCode(String code) throws Exception;

	public String addState(State state) throws Exception;

	public void updateState(State state) throws Exception;

	public Long getStatesCount() throws Exception;

}
