/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.Village;

/**
 * @author Rohit
 *
 */
public interface VillageDao {

	public List<Village> getVillages(int page, int limit) throws Exception;

	public Village getVillageByCode(String code) throws Exception;

	public String addVillage(Village taluka) throws Exception;

	public void updateVillage(Village taluka) throws Exception;

	public Long getVillagesCount() throws Exception;

}
