/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.MstUsers;
import com.nath.dto.UserValidateDTO;

/**
 * @author Rohit
 *
 */
public interface UserDao {

	public MstUsers authenticateUser(UserValidateDTO validateDTO) throws Exception;

	public List<MstUsers> getUsers(int page, int limit) throws Exception;

	public MstUsers getUserById(int id) throws Exception;

	public Integer addUsers(MstUsers users) throws Exception;

	public void updateUsers(MstUsers users) throws Exception;

	public Long getUsersCount() throws Exception;
	
	public MstUsers validateUsername(String username) throws Exception;
}
