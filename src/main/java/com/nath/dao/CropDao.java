/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.Crop;

/**
 * @author Rohit
 *
 */
public interface CropDao {

	public List<Crop> getCrops(int page, int limit) throws Exception;

	public Crop getCropById(String code) throws Exception;

	public String addCrop(Crop crop) throws Exception;

	public void updateCrop(Crop crop) throws Exception;

	public Long getCropsCount() throws Exception;

}
