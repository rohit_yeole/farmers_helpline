/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.Taluka;

/**
 * @author Rohit
 *
 */
public interface TalukaDao {

	public List<Taluka> getTalukas(int page, int limit) throws Exception;

	public List<Taluka> getTalukasByDistrictCode(String districtCode) throws Exception;

	public Taluka getTalukaByCode(String code) throws Exception;

	public String addTaluka(Taluka taluka) throws Exception;

	public void updateTaluka(Taluka taluka) throws Exception;

	public Long getTalukasCount() throws Exception;

}
