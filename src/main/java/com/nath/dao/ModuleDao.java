/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.MstModules;

/**
 * @author Rohit
 *
 */
public interface ModuleDao {

	public List<MstModules> getModules(int page, int limit) throws Exception;

	public MstModules getModulesById(int id) throws Exception;

	public String addModules(MstModules modules) throws Exception;

	public void updateModules(MstModules modules) throws Exception;

	public Long getModulesCount() throws Exception;
}
