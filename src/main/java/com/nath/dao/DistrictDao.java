/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.District;

/**
 * @author Rohit
 *
 */
public interface DistrictDao {

	public List<District> getDistricts(int page, int limit) throws Exception;

	public List<District> getDistrictsByStateCode(String stateCode) throws Exception;

	public District getDistrictByCode(String code) throws Exception;

	public String addDistrict(District district) throws Exception;

	public void updateDistrict(District district) throws Exception;

	public Long getDistrictsCount() throws Exception;

}
