/**
 * 
 */
package com.nath.dao;

import java.util.List;

import com.nath.bo.Varient;

/**
 * @author Rohit
 *
 */
public interface VarientDao {

	public List<Varient> getVarients(int page, int limit) throws Exception;

	public List<Varient> getVarientsByCropCode(String cropCode) throws Exception;

	public Varient getVarientByCode(String code) throws Exception;

	public String addVarient(Varient varient) throws Exception;

	public void updateVarient(Varient varient) throws Exception;

	public Long getVarientsCount() throws Exception;

	public Varient getVarientByCropCodeVarientCode(String cropCode, String varientCode) throws Exception;

}
