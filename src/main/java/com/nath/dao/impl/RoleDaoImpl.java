package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.MstRoles;
import com.nath.dao.RoleDao;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Long getRolesCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession().createQuery("select count(*) from MstRoles  where isDeleted = 0")
					.uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

	@Override
	public List<MstRoles> getRoles(int page, int limit) throws Exception {
		List<MstRoles> list = new ArrayList<MstRoles>();
		try {
			list = (List<MstRoles>) sessionFactory.getCurrentSession().createQuery("from MstRoles where isDeleted = 0").setFirstResult(page)
					.setMaxResults(limit).list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return list;
	}

	@Override
	public MstRoles getRolesById(int id) throws Exception {
		MstRoles role = new MstRoles();

		try {
			role = (MstRoles) sessionFactory.getCurrentSession().createQuery("from MstRoles where roleId = :id")
					.setParameter("id", id).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return role;
	}

	@Override
	public String addRoles(MstRoles role) throws Exception {
		String id = null;
		try {
			Integer id1 = (Integer) sessionFactory.getCurrentSession().save(role);
			id = id1.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateRoles(MstRoles role) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(role);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}
}
