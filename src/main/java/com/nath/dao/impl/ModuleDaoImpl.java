package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.MstModules;
import com.nath.dao.ModuleDao;

@Repository
public class ModuleDaoImpl implements ModuleDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Long getModulesCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession().createQuery("select count(*) from MstModules where isDeleted = 0")
					.uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

	@Override
	public List<MstModules> getModules(int page, int limit) throws Exception {
		List<MstModules> list = new ArrayList<MstModules>();
		try {
			list = (List<MstModules>) sessionFactory.getCurrentSession().createQuery("from MstModules  where isDeleted = 0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return list;
	}

	@Override
	public MstModules getModulesById(int id) throws Exception {
		MstModules module = new MstModules();

		try {
			module = (MstModules) sessionFactory.getCurrentSession().createQuery("from MstModules where moduleId = :id")
					.setParameter("id", id).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return module;
	}

	@Override
	public String addModules(MstModules module) throws Exception {
		String id = null;
		try {
			Integer id1 = (Integer) sessionFactory.getCurrentSession().save(module);
			id = id1.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateModules(MstModules module) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(module);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}
}
