package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.Crop;
import com.nath.dao.CropDao;

@Repository
public class CropDaoImpl implements CropDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Crop> getCrops(int page, int limit) throws Exception {
		List<Crop> list = new ArrayList<Crop>();
		try {
			list = (List<Crop>) sessionFactory.getCurrentSession().createQuery("from Crop where isDeleted=0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return list;
	}

	@Override
	public Crop getCropById(String code) throws Exception {
		Crop crop = new Crop();

		try {
			crop = (Crop) sessionFactory.getCurrentSession().createQuery("from Crop where cropCode = :code")
					.setParameter("code", code).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return crop;
	}

	@Override
	public String addCrop(Crop crop) throws Exception {
		String id = null;
		try {
			id = (String) sessionFactory.getCurrentSession().save(crop);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateCrop(Crop crop) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(crop);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}
	
	@Override
	public Long getCropsCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from Crop where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
