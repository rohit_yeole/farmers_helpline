package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.Taluka;
import com.nath.dao.TalukaDao;

@Repository
public class TalukaDaoImpl implements TalukaDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Taluka> getTalukas(int page, int limit) throws Exception {
		List<Taluka> talukas = new ArrayList<Taluka>();
		try {
			talukas = (List<Taluka>) sessionFactory.getCurrentSession().createQuery("from Taluka where isDeleted = 0 ")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return talukas;
	}

	@Override
	public List<Taluka> getTalukasByDistrictCode(String districtCode) throws Exception {
		List<Taluka> talukas = new ArrayList<Taluka>();
		try {
			talukas = (List<Taluka>) sessionFactory.getCurrentSession()
					.createQuery("from Taluka where isDeleted = 0 and district.districtCode = :code ")
					.setParameter("code", districtCode).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return talukas;
	}

	@Override
	public Taluka getTalukaByCode(String code) throws Exception {
		Taluka taluka;
		try {
			taluka = (Taluka) sessionFactory.getCurrentSession().createQuery("from Taluka where talukaCode = :code")
					.setParameter("code", code).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return taluka;
	}

	@Override
	public String addTaluka(Taluka taluka) throws Exception {
		String id;
		try {
			id = (String) sessionFactory.getCurrentSession().save(taluka);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateTaluka(Taluka taluka) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(taluka);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public Long getTalukasCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from Taluka where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
