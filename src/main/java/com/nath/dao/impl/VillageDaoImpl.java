package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.Village;
import com.nath.dao.VillageDao;

@Repository
public class VillageDaoImpl implements VillageDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Village> getVillages(int page, int limit) throws Exception {
		List<Village> villages = new ArrayList<Village>();
		try {
			villages = (List<Village>) sessionFactory.getCurrentSession()
					.createQuery("from Village where isDeleted = 0 ").list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return villages;
	}

	@Override
	public Village getVillageByCode(String code) throws Exception {
		Village village;
		try {
			village = (Village) sessionFactory.getCurrentSession().createQuery("from Village where villageCode = :code")
					.setParameter("code", code).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return village;
	}

	@Override
	public String addVillage(Village village) throws Exception {
		String id;
		try {
			id = (String) sessionFactory.getCurrentSession().save(village);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateVillage(Village village) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(village);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	
	@Override
	public Long getVillagesCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from Village where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
