package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.Branch;
import com.nath.dao.BranchDao;

@Repository
public class BranchDaoImpl implements BranchDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Branch> getBranches(int page, int limit) throws Exception {
		List<Branch> list = new ArrayList<Branch>();
		try {
			list = (List<Branch>) sessionFactory.getCurrentSession().createQuery("from Branch where isDeleted=0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return list;
	}

	@Override
	public Branch getBranchByCode(String code) throws Exception {
		Branch branch = new Branch();

		try {
			branch = (Branch) sessionFactory.getCurrentSession().createQuery("from Branch where branchCode = :code")
					.setParameter("code", code).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return branch;
	}

	@Override
	public String addBranch(Branch branch) throws Exception {
		String id = null;
		try {
			id = (String) sessionFactory.getCurrentSession().save(branch);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateBranch(Branch branch) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(branch);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public Long getBranchesCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from Branch where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
