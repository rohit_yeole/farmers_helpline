package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.Varient;
import com.nath.dao.VarientDao;

@Repository
public class VarientDaoImpl implements VarientDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Varient> getVarients(int page, int limit) throws Exception {
		List<Varient> varients = new ArrayList<Varient>();
		try {
			varients = (List<Varient>) sessionFactory.getCurrentSession().createQuery("from Varient where isDeleted=0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return varients;
	}

	@Override
	public List<Varient> getVarientsByCropCode(String cropCode) throws Exception {
		List<Varient> varients = new ArrayList<Varient>();
		try {
			varients = (List<Varient>) sessionFactory.getCurrentSession()
					.createQuery("from Varient where isDeleted=0 and crop.cropCode = :code ")
					.setParameter("code", cropCode).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return varients;
	}

	@Override
	public Varient getVarientByCode(String code) throws Exception {
		Varient varient;
		try {
			varient = (Varient) sessionFactory.getCurrentSession().createQuery("from Varient where varientCode = :code")
					.setParameter("code", code).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return varient;
	}

	@Override
	public Varient getVarientByCropCodeVarientCode(String cropCode, String varientCode) throws Exception {
		Varient varient;
		try {
			varient = (Varient) sessionFactory.getCurrentSession()
					.createQuery("from Varient where crop.cropCode = :cropCode and varientCode = :varientCode")
					.setParameter("cropCode", cropCode).setParameter("varientCode", varientCode).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return varient;
	}

	@Override
	public String addVarient(Varient varient) throws Exception {
		String id;
		try {
			id = (String) sessionFactory.getCurrentSession().save(varient);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;

	}

	@Override
	public void updateVarient(Varient varient) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(varient);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public Long getVarientsCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from Varient where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
