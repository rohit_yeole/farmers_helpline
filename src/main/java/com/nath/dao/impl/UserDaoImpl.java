package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.MstUsers;
import com.nath.dao.UserDao;
import com.nath.dto.UserValidateDTO;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public MstUsers authenticateUser(UserValidateDTO validateDTO) throws Exception {
		MstUsers mstUsers;
		try {
			mstUsers = (MstUsers) sessionFactory.getCurrentSession()
					.createQuery("from MstUsers where userName = :username and password = :password and isDeleted = 0")
					.setParameter("username", validateDTO.getUsername())
					.setParameter("password", validateDTO.getPassword()).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return mstUsers;
	}

	@Override
	public Long getUsersCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from MstUsers where isDeleted = 0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

	@Override
	public List<MstUsers> getUsers(int page, int limit) throws Exception {
		List<MstUsers> list = new ArrayList<MstUsers>();
		try {
			list = (List<MstUsers>) sessionFactory.getCurrentSession().createQuery("from MstUsers  where isDeleted = 0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return list;
	}

	@Override
	public MstUsers getUserById(int id) throws Exception {
		MstUsers user = new MstUsers();

		try {
			user = (MstUsers) sessionFactory.getCurrentSession().createQuery("from MstUsers where userId = :id")
					.setParameter("id", id).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return user;
	}

	@Override
	public Integer addUsers(MstUsers user) throws Exception {
		Integer id = null;
		try {
			id = (Integer) sessionFactory.getCurrentSession().save(user);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateUsers(MstUsers user) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(user);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public MstUsers validateUsername(String username) throws Exception {
		MstUsers user = new MstUsers();

		try {
			user = (MstUsers) sessionFactory.getCurrentSession().createQuery("from MstUsers where userName = :username")
					.setParameter("username", username).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return user;
	}
}
