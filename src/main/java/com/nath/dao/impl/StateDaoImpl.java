package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.State;
import com.nath.dao.StateDao;

@Repository
public class StateDaoImpl implements StateDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<State> getStates(int page, int limit) throws Exception {
		List<State> states = new ArrayList<State>();
		try {
			states = (List<State>) sessionFactory.getCurrentSession().createQuery("from State where isDeleted=0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return states;
	}

	@Override
	public List<State> getStatesByCountryCode(String countryCode) throws Exception {
		List<State> states = new ArrayList<State>();
		try {
			states = (List<State>) sessionFactory.getCurrentSession()
					.createQuery("from State where isDeleted=0 and country.countryCode = :code ")
					.setParameter("code", countryCode).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return states;
	}

	@Override
	public State getStateByCode(String code) throws Exception {
		State state;
		try {
			state = (State) sessionFactory.getCurrentSession().createQuery("from State where stateCode = :code")
					.setParameter("code", code).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return state;
	}

	@Override
	public String addState(State state) throws Exception {
		String id;
		try {
			id = (String) sessionFactory.getCurrentSession().save(state);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;

	}

	@Override
	public void updateState(State state) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(state);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public Long getStatesCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from State where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
