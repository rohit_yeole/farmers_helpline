package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.District;
import com.nath.dao.DistrictDao;

@Repository
public class DistrictDaoImpl implements DistrictDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<District> getDistricts(int page, int limit) throws Exception {
		List<District> districts = new ArrayList<District>();
		try {
			districts = (List<District>) sessionFactory.getCurrentSession()
					.createQuery("from District where isDeleted=0").setFirstResult(page).setMaxResults(limit).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return districts;
	}

	@Override
	public List<District> getDistrictsByStateCode(String stateCode) throws Exception {
		List<District> districts = new ArrayList<District>();
		try {
			districts = (List<District>) sessionFactory.getCurrentSession()
					.createQuery("from District where isDeleted=0 and state.stateCode = :code")
					.setParameter("code", stateCode).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return districts;
	}

	@Override
	public District getDistrictByCode(String code) throws Exception {
		District district;
		try {
			district = (District) sessionFactory.getCurrentSession()
					.createQuery("from District where districtCode = :code").setParameter("code", code).uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return district;
	}

	@Override
	public String addDistrict(District district) throws Exception {
		String id;
		try {
			id = (String) sessionFactory.getCurrentSession().save(district);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateDistrict(District district) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(district);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	
	@Override
	public Long getDistrictsCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from District where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
