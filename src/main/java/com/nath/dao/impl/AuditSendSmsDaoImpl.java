package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.nath.model.AuditSendSms;
import com.nath.model.AuditSendSmsMapper;
import com.nath.model.FarmerModel;
import com.nath.model.FarmersStgMapper;

@Repository
public class AuditSendSmsDaoImpl {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	public List<AuditSendSms> getAuditSendSmsData(){
		
		List<AuditSendSms> auditList=new ArrayList<AuditSendSms>();
		
		
		String sql="SELECT * FROM audit_send_sms";
		
		auditList=jdbcTemplate.query(sql,new AuditSendSmsMapper());
		
		
		return auditList;
	}
	
	public int insert(AuditSendSms ass) {
		
		

		String query ="INSERT INTO  audit_send_sms  ( user_id , mobile_numbers , sms , created_by  )"
				+ " VALUES (?,?,?,?)";
		
		int result =jdbcTemplate.update(query,ass.getUserID(),ass.getMobileNumber(),ass.getSms(),1);
		
		
		
		return result;
		
		
	}

}
