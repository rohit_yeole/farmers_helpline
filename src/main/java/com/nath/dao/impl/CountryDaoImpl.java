package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.Country;
import com.nath.dao.CountryDao;

@Repository
public class CountryDaoImpl implements CountryDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Country> getCountries(int page, int limit) throws Exception {
		List<Country> list = new ArrayList<Country>();
		try {
			list = (List<Country>) sessionFactory.getCurrentSession().createQuery("from Country where isDeleted=0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return list;
	}

	@Override
	public Country getCountryById(String code) throws Exception {
		Country country = new Country();

		try {
			country = (Country) sessionFactory.getCurrentSession().createQuery("from Country where countryCode = :code")
					.setParameter("code", code).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return country;
	}

	@Override
	public String addCountry(Country country) throws Exception {
		String id = null;
		try {
			id = (String) sessionFactory.getCurrentSession().save(country);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateCountry(Country country) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(country);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public Long getCountriesCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from Country where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
