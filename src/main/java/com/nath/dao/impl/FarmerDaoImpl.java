package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nath.bo.Crop;
import com.nath.bo.Farmer;
import com.nath.dao.FarmerDao;
import com.nath.dto.FarmerSearchDTO;

@Repository
public class FarmerDaoImpl implements FarmerDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Farmer> getFarmers(int page, int limit) throws Exception {
		List<Farmer> farmers = new ArrayList<Farmer>();
		try {
			farmers = (List<Farmer>) sessionFactory.getCurrentSession().createQuery("from Farmer where isDeleted=0")
					.setFirstResult(page).setMaxResults(limit).list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return farmers;
	}

	@Override
	public Farmer getFarmerById(Integer id) throws Exception {
		Farmer farmer;
		try {
			farmer = sessionFactory.getCurrentSession().get(Farmer.class, id);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return farmer;
	}
	
	@Override
	public Farmer getFarmerByMobile(String mobile) throws Exception {
		Farmer farmer = new Farmer();

		try {
			farmer = (Farmer) sessionFactory.getCurrentSession().createQuery("from Farmer where mobileNo = :mobile")
					.setParameter("mobile", mobile).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return farmer;
	}

	@Override
	public Integer addFarmer(Farmer farmer) throws Exception {
		Integer id;
		try {
			id = (Integer) sessionFactory.getCurrentSession().save(farmer);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public void updateFarmer(Farmer farmer) throws Exception {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(farmer);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public List<Farmer> getFarmersByFilter(FarmerSearchDTO farmerSearchDTO) throws Exception {
		List<Farmer> farmers = new ArrayList<Farmer>();
		try {
			String query = "select f from Farmer f left join f.farmerVarientMappings fvm";
			if (null != farmerSearchDTO.getStateCode() && !farmerSearchDTO.getStateCode().isEmpty()) {
				query = containsWhere(query) + " f.taluka.district.state.stateCode = '" + "'";
			}
			if (null != farmerSearchDTO.getDistrictCode() && !farmerSearchDTO.getDistrictCode().isEmpty()) {
				query = containsWhere(query) + " f.taluka.district.districtCode = '" + farmerSearchDTO.getDistrictCode()
						+ "'";
			}
			if (null != farmerSearchDTO.getTalukaCode() && !farmerSearchDTO.getTalukaCode().isEmpty()) {
				query = containsWhere(query) + " f.taluka.talukaCode = '" + farmerSearchDTO.getTalukaCode() + "'";
			}
			if (null != farmerSearchDTO.getVillageName() && !farmerSearchDTO.getVillageName().isEmpty()) {
				query = containsWhere(query) + " f.villageName = '" + farmerSearchDTO.getVillageName() + "'";
			}
			if (null != farmerSearchDTO.getBranchCode() && !farmerSearchDTO.getBranchCode().isEmpty()) {
				query = containsWhere(query) + " f.branch.branchCode = '" + farmerSearchDTO.getBranchCode() + "'";
			}
			if (null != farmerSearchDTO.getCropCode() && !farmerSearchDTO.getCropCode().isEmpty()) {
				query = containsWhere(query) + " f.farmerId = fvm.farmer.farmerId and fvm.varient.crop.cropCode = '"
						+ farmerSearchDTO.getCropCode() + "'";
			}
			if (null != farmerSearchDTO.getVarientCode() && !farmerSearchDTO.getVarientCode().isEmpty()) {
				query = containsWhere(query) + " f.farmerId = fvm.farmer.farmerId and fvm.varient.varientCode = '"
						+ farmerSearchDTO.getVarientCode() + "'";
			}
			System.out.println("Query : " + query.concat(" and f.isDeleted=0"));
			Query q = sessionFactory.getCurrentSession().createQuery(query);
			farmers = (List<Farmer>) q.list();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return farmers;
	}

	private String containsWhere(String query) {
		if (query.contains("where"))
			query = query.concat(" and ");
		else
			query = query.concat(" where ");
		return query;
	}

	@Override
	public Long getFarmersCount() throws Exception {
		Long count;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createQuery("select count(*) from Farmer where isDeleted=0").uniqueResult();
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
