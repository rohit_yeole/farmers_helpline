package com.nath.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mysql.cj.jdbc.CallableStatement;
import com.nath.bo.Crop;
import com.nath.bo.Farmer;
import com.nath.bo.FarmerVarientMapping;
import com.nath.bo.Varient;
import com.nath.dao.CropDao;
import com.nath.dao.FarmerDao;
import com.nath.dao.VarientDao;
import com.nath.model.DistrictStgModel;
import com.nath.model.DistrictStgModelMapper;
//import com.mysql.jdbc.CallableStatement;
import com.nath.model.FarmerModel;
import com.nath.model.FarmersStgMapper;
import com.nath.model.FileUploadRequest;
import com.nath.model.FileUploadRequestMapper;
import com.nath.model.ReportRequestModel;
import com.nath.model.TalukaStgModel;
import com.nath.model.TalukaStgModelMapper;
import com.nath.model.VarientStgModel;
import com.nath.model.VarientStgModelMapper;
import com.nath.model.VarientStgResponseModel;

/*
 * Yogesh Somvanshi
 */
@Repository

public class FileDaoImpl {

	@Autowired
	CropDao cropDao;

	@Autowired
	FarmerDao farmerDao;

	@Autowired
	VarientDao varientDao;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	// @Override
	public void insert(List<FarmerModel> paramT, int reuestId) throws InterruptedException {
		// TODO Auto-generated method stub
		String SQL = "INSERT INTO  farmer_stg  ( mobile_no , name , village_name , tq , district , state , win_branch , branch , digit,requestId ) "
				+ " VALUES(?,?,?,?,?,?,?,?,?,?)";

		for (FarmerModel farmerModel : paramT) {

			int result = jdbcTemplate.update(SQL, farmerModel.getMobileNo(), farmerModel.getFarmerName(),
					farmerModel.getVillage(), farmerModel.getTaluka(), farmerModel.getDistrict(),
					farmerModel.getState(), farmerModel.getWinBranch(), farmerModel.getBranch(), farmerModel.getDigit(),
					reuestId);
			// Thread.sleep(1 * 100);
		}

		// validate();

		// return true;
	}

	public void insertDistrictStg(List<DistrictStgModel> paramT, int reuestId) throws InterruptedException {
		// TODO Auto-generated method stub
		String SQL = "INSERT INTO  district_stg  ( district_code , district_name , state_code , state_name,request_id ) "
				+ " VALUES(?,?,?,?,?)";

		for (DistrictStgModel districtModel : paramT) {

			int result = jdbcTemplate.update(SQL, districtModel.getDistCode(), districtModel.getDistName(),
					districtModel.getStateCode(), districtModel.getStateName(), reuestId);
			// Thread.sleep(1 * 100);
		}

	}

	public void insertTalukaStg(List<TalukaStgModel> paramT, int reuestId) throws InterruptedException {
		// TODO Auto-generated method stub
		String SQL = "INSERT INTO  taluka_stg  ( taluka_code , taluka_name , district_code , district_name,request_id ) "
				+ " VALUES(?,?,?,?,?)";

		for (TalukaStgModel talukaStgModel : paramT) {

			int result = jdbcTemplate.update(SQL, talukaStgModel.getTalukaCode(), talukaStgModel.getTalukaName(),
					talukaStgModel.getDistrictCode(), talukaStgModel.getDistrictName(), reuestId);
			// Thread.sleep(1 * 100);
		}

	}

	public void insertVarientStg(List<VarientStgModel> paramT, int requestId) throws InterruptedException {
		// TODO Auto-generated method stub
		String SQL = "INSERT INTO  varient_stg  ( mobileNo , cropCode , varientCode , request_id ) "
				+ " VALUES(?,?,?,?)";

		for (VarientStgModel varientStgModel : paramT) {

			int result = jdbcTemplate.update(SQL, varientStgModel.getMobileNo(), varientStgModel.getCropCode(),
					varientStgModel.getVarientCode(), requestId);
			// Thread.sleep(1 * 100);
		}

	}

	public void insertDataDistrict(int reuestId) {

		String sql = "select * from district_stg where request_id=" + reuestId;
		String sqlUpdateDistrictStgState = " update district_stg set log=? where id =?";
		String sqldist = "";
		String sqlStateCheck = "select count(*) from state where state_code=?";
		String sqlDistrictCheck = "select count(*) from district where district_code=?";
		String sqldistupdate = "update district set district_name=? where district_code=?";
		String sqldistinsert = "insert into district (district_code,district_name,state_code) values(?,?,?);";

		ArrayList<DistrictStgModel> dmlist = (ArrayList<DistrictStgModel>) jdbcTemplate.query(sql,
				new DistrictStgModelMapper());

		for (DistrictStgModel districtStgModel : dmlist) {

			System.out.println(" districtStgModel" + districtStgModel.toString());
			int count = getJdbcTemplate().queryForObject(sqlStateCheck,
					new Object[] { districtStgModel.getStateCode() }, Integer.class);

			if (count == 1) {
				int distCount = getJdbcTemplate().queryForObject(sqlDistrictCheck,
						new Object[] { districtStgModel.getDistCode() }, Integer.class);

				if (distCount == 1) {
					jdbcTemplate.update(sqldistupdate, districtStgModel.getDistName(), districtStgModel.getDistCode());

				} else {
					// Insert data

					jdbcTemplate.update(sqldistinsert, districtStgModel.getDistCode(), districtStgModel.getDistName(),
							districtStgModel.getStateCode());

				}

			} else {
				jdbcTemplate.update(sqlUpdateDistrictStgState, "State code is not in master", districtStgModel.getId());
			}

		}

	}

	public List<VarientStgModel> getVarientListFromStg(int requestID) {

		String sql = "select * from varient_stg where request_id=" + requestID;

		List<VarientStgModel> tmlist = (ArrayList<VarientStgModel>) jdbcTemplate.query(sql,
				new VarientStgModelMapper());
		return (null != tmlist ? tmlist : new ArrayList<VarientStgModel>());
	}
	
	public void updateVarientListInStg(List<VarientStgResponseModel> stgResponseModelList) {
		
		for(VarientStgResponseModel model:stgResponseModelList) {
			
			String sqlUpdateTalukaStgDistrict = "update varient_stg set log=? where id =?";
			
			try {
				int row = jdbcTemplate.update(sqlUpdateTalukaStgDistrict, model.getComment(),
						model.getId());
				System.out.println("Row Updated : "+row);
			} catch (DataAccessException e) {
				e.printStackTrace();
			}
		}

	}

	public void insertDataTaluka(int reuestId) {

		String sql = "select * from taluka_stg where request_id=" + reuestId;

		String sqlUpdateTalukaStgDistrict = "update taluka_stg set log=? where id =?";
		// String sqldist ="";
		String sqlDistrictCheck = "select count(*) from district where district_code=?";
		String sqlTalukaCheck = "select count(*) from taluka where taluka_code=?";

		String sqlTalukaupdate = "update taluka set taluka_name=? where taluka_code=?";
		String sqlTalukainsert = "insert into taluka (taluka_code,taluka_name,district_code) values(?,?,?);";

		ArrayList<TalukaStgModel> tmlist = (ArrayList<TalukaStgModel>) jdbcTemplate.query(sql,
				new TalukaStgModelMapper());

		for (TalukaStgModel talukaStgModel : tmlist) {

			System.out.println(" talukaStgModel" + talukaStgModel.toString());
			int count = getJdbcTemplate().queryForObject(sqlDistrictCheck,
					new Object[] { talukaStgModel.getDistrictCode() }, Integer.class);

			System.out.println(count + " -- District Count DistrictCode()   : " + talukaStgModel.getDistrictCode());
			if (count == 1) {
				int distCount = getJdbcTemplate().queryForObject(sqlTalukaCheck,
						new Object[] { talukaStgModel.getTalukaCode() }, Integer.class);

				if (distCount == 1) {
					jdbcTemplate.update(sqlTalukaupdate, talukaStgModel.getTalukaName(),
							talukaStgModel.getTalukaCode());

					jdbcTemplate.update(sqlUpdateTalukaStgDistrict, "Taluka Data Updated", talukaStgModel.getId());

				} else {
					// Insert data

					jdbcTemplate.update(sqlTalukainsert, talukaStgModel.getTalukaCode(), talukaStgModel.getTalukaName(),
							talukaStgModel.getDistrictCode());

					jdbcTemplate.update(sqlUpdateTalukaStgDistrict, "Taluka Data Added", talukaStgModel.getId());

				}

			} else {
				try {
					jdbcTemplate.update(sqlUpdateTalukaStgDistrict, "District code is not in District Master",
							talukaStgModel.getId());

				} catch (Exception e) {
					// TODO: handle exception
				}
			}

		}

		System.out.println(" insertDataTaluka");

	}

	public int insertReuest(ReportRequestModel reportModule, String fileType) {

		String SQL = "INSERT INTO file_upload_request ( user_id , file_name , status,file_upload_type ) VALUES (?,?,?,?)";

		int result = jdbcTemplate.update(SQL, 1, reportModule.getFileName(), reportModule.getStatus(), fileType);
		int count;

		String requestid = "SELECT LAST_INSERT_ID()";
		count = jdbcTemplate.queryForObject(requestid, Integer.class);

		return count;
	}

	public void updateRequestStatus(int requestId) {

		String SQL = "update file_upload_request set status='Completed' where request_id=" + requestId;

		int result = jdbcTemplate.update(SQL);

		// return count;
	}

	public List<FarmerModel> getFileuploadedData(int requestId) {

		String sql = "SELECT * FROM farmer_stg where requestId=" + requestId;

		ArrayList<FarmerModel> fmList = (ArrayList<FarmerModel>) jdbcTemplate.query(sql, new FarmersStgMapper());
		return fmList;

	}

	public void insert(List<FarmerModel> paramT) throws InterruptedException {
		// TODO Auto-generated method stub
		String SQL = "INSERT INTO  farmer_stg  ( mobile_no , name , village_name , tq , district , state , win_branch , branch , digit ) "
				+ " VALUES(?,?,?,?,?,?,?,?,?)";
		int result = 0;
		for (FarmerModel farmerModel : paramT) {
			System.out.println(farmerModel.getMobileNo().length() + " - | - " + farmerModel.getMobileNo());
			if (farmerModel.getMobileNo().length() < 0) {
				result = jdbcTemplate.update(SQL, farmerModel.getMobileNo(), farmerModel.getFarmerName(),
						farmerModel.getVillage(), farmerModel.getTaluka(), farmerModel.getDistrict(),
						farmerModel.getState(), farmerModel.getWinBranch(), farmerModel.getBranch(),
						farmerModel.getDigit());
			}
			Thread.sleep(1 * 100);
		}

		// validate();

		// return true;
	}

	public void validate(int requestID) {
		System.out.println(" In validation ");
		String validateData = "{call ValidateFarmer(" + requestID + " )}";
		CallableStatement callableStatement = null;
		jdbcTemplate.execute(validateData);

	}

	public void insertDataintoFarmer(int requestID) {

		System.out.println(" In insertDataintoFarmer ");
		String validateData = "{call EnterDataToFarmer(" + requestID + " )}";
		CallableStatement callableStatement = null;
		jdbcTemplate.execute(validateData);
	}

	public List<FileUploadRequest> getListoffileuplod(String fileType) {

		List<FileUploadRequest> fileUploadList = new ArrayList<FileUploadRequest>();
		String sql = "SELECT * FROM file_upload_request where file_upload_type='" + fileType + "'";
		fileUploadList = jdbcTemplate.query(sql, new FileUploadRequestMapper());

		return fileUploadList;

	}

	@Transactional
	public List<VarientStgResponseModel> insertDataVarient(List<VarientStgModel> list) {

		List<VarientStgResponseModel> stgResponseList = new ArrayList<VarientStgResponseModel>();

		List<FarmerVarientMapping> farmerVarientMappings = new ArrayList<FarmerVarientMapping>();
		FarmerVarientMapping cropMapping = new FarmerVarientMapping();
		VarientStgResponseModel stgResponseModel ;
		for (VarientStgModel model : list) {
			stgResponseModel = new VarientStgResponseModel();
			stgResponseModel.setId(model.getId());
			stgResponseModel.setMobileNo(model.getMobileNo());
			stgResponseModel.setCropCode(model.getCropCode());
			stgResponseModel.setVarientCode(model.getVarientCode());

			try {
				Farmer farmer = farmerDao.getFarmerByMobile(model.getMobileNo());

				if (null != farmer) {
					farmer.getFarmerVarientMappings().clear();
					String cropCode = model.getCropCode();

					Crop crop = null;
					try {
						crop = cropDao.getCropById(cropCode);
						if (null != crop) {
							boolean valid = true;
							String[] varientArr = model.getVarientCode().split(",");
							Varient varient;
							for (String varientCode : varientArr) {

								varient = varientDao.getVarientByCropCodeVarientCode(cropCode, varientCode);
								if (null != varient) {
									cropMapping = new FarmerVarientMapping();
									cropMapping.setFarmer(farmer);
									cropMapping.setVarient(varient);
									if (!farmerVarientMappings.contains(cropMapping))
										farmerVarientMappings.add(cropMapping);
									farmer.getFarmerVarientMappings().add(cropMapping);
									valid = true;
								} else {
									stgResponseModel.setComment("Invalid Crop-Varient Code combination");
									stgResponseList.add(stgResponseModel);
									valid = false;
									break;
								}
							}
							if (valid) {
								farmer.getFarmerVarientMappings().remove(null);
								farmerDao.updateFarmer(farmer);
							}
						} else {
							stgResponseModel.setComment("Invalid Crop Code");
							stgResponseList.add(stgResponseModel);
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {
					stgResponseModel.setComment("Invalid Mobile No");
					stgResponseList.add(stgResponseModel);
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return stgResponseList;

	}
}