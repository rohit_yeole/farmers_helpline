package com.nath.model;

public class VarientStgModel {

	private long id;
	private String mobileNo;
	private String cropCode;
	private String varientCode;
	private String log;
	private int requestId;

	public VarientStgModel() {
		super();
	}

	public VarientStgModel(long id, String mobileNo, String cropCode, String varientCode, String log, int requestId) {
		super();
		this.id = id;
		this.mobileNo = mobileNo;
		this.cropCode = cropCode;
		this.varientCode = varientCode;
		this.log = log;
		this.requestId = requestId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCropCode() {
		return cropCode;
	}

	public void setCropCode(String cropCode) {
		this.cropCode = cropCode;
	}

	public String getVarientCode() {
		return varientCode;
	}

	public void setVarientCode(String varientCode) {
		this.varientCode = varientCode;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

}
