package com.nath.model;

import java.util.Date;

public class FileUploadRequest {

	private int requestId;
	private int userId;
	private Date requestDate;
	private String fileName;
	private String status;
	
	public FileUploadRequest(int requestId, int userId, Date requestDate, String fileName, String status) {
		super();
		this.requestId = requestId;
		this.userId = userId;
		this.requestDate = requestDate;
		this.fileName = fileName;
		this.status = status;
	}
	
	public FileUploadRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "FileUploadRequest [requestId=" + requestId + ", userId=" + userId + ", requestDate=" + requestDate
				+ ", fileName=" + fileName + ", status=" + status + "]";
	}
	
	
	
	
}
