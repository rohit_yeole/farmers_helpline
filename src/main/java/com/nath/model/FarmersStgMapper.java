package com.nath.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class FarmersStgMapper implements RowMapper<FarmerModel> {

	@Override
	public FarmerModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		FarmerModel fm =new FarmerModel();
		
		fm.setNo(rs.getString("id"));
		fm.setMobileNo(rs.getString("mobile_no"));
		fm.setFarmerName(rs.getString("name"));
		fm.setVillage(rs.getString("village_name"));
		fm.setTaluka(rs.getString("tq"));
		fm.setDistrict(rs.getString("district"));
		fm.setState(rs.getString("state"));
		fm.setWinBranch(rs.getString("win_branch"));
		fm.setBranch(rs.getString("branch"));
		fm.setDigit(rs.getString("digit"));
		fm.setLog(rs.getString("log"));
		fm.setRequestId(rs.getInt("requestId"));
		return fm;
	}

	
	

}
