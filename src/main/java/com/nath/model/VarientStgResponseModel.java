package com.nath.model;

public class VarientStgResponseModel {

	private long id;
	private String mobileNo;
	private String cropCode;
	private String varientCode;
	private String comment;

	public VarientStgResponseModel() {
		super();
	}

	public VarientStgResponseModel(long id, String mobileNo, String cropCode, String varientCode, String comment) {
		super();
		this.id = id;
		this.mobileNo = mobileNo;
		this.cropCode = cropCode;
		this.varientCode = varientCode;
		this.comment = comment;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCropCode() {
		return cropCode;
	}

	public void setCropCode(String cropCode) {
		this.cropCode = cropCode;
	}

	public String getVarientCode() {
		return varientCode;
	}

	public void setVarientCode(String varientCode) {
		this.varientCode = varientCode;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
