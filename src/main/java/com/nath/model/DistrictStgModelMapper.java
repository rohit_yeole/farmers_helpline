package com.nath.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DistrictStgModelMapper implements RowMapper<DistrictStgModel> {

	@Override
	public DistrictStgModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		DistrictStgModel dm=new DistrictStgModel();
		
		
		
		dm.setId(rs.getLong("id"));
		dm.setDistCode(rs.getString("district_code"));
		dm.setDistName(rs.getString("district_name"));
		dm.setStateCode(rs.getString("state_code"));
		dm.setStateName(rs.getString("state_name"));
		dm.setLog(rs.getString("log"));
		dm.setRequestId(rs.getInt("request_id"));
		return dm;
	}

}
