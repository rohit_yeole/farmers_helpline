package com.nath.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TalukaStgModelMapper implements RowMapper<TalukaStgModel> {

	@Override
	public TalukaStgModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		TalukaStgModel tm=new TalukaStgModel();
		
		
		tm.setId(rs.getLong("id"));
		tm.setTalukaCode(rs.getString("taluka_code"));
		tm.setTalukaName(rs.getString("taluka_name"));
		tm.setDistrictCode(rs.getString("district_code"));
		tm.setDistrictName(rs.getString("district_name"));
		tm.setLog(rs.getString("log"));
		tm.setRequestId((rs.getInt("request_id")));
		
		return tm;
	}

}
