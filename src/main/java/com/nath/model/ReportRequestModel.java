package com.nath.model;

import java.sql.Timestamp;

public class ReportRequestModel {

	
	private int requestId;
	private int userId;
	private Timestamp request;	
	private String	fileName;
	private String status;
	
	public ReportRequestModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReportRequestModel(int requestId, int userId, Timestamp request, String fileName, String status) {
		super();
		this.requestId = requestId;
		this.userId = userId;
		this.request = request;
		this.fileName = fileName;
		this.status = status;
	}

	
	
	public ReportRequestModel(int userId, String fileName, String status) {
		super();
		this.userId = userId;
		
		this.fileName = fileName;
		this.status = status;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Timestamp getRequest() {
		return request;
	}

	public void setRequest(Timestamp request) {
		this.request = request;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ReportRequestModel [requestId=" + requestId + ", userId=" + userId + ", request=" + request
				+ ", fileName=" + fileName + ", status=" + status + "]";
	}
	
	
	
}
