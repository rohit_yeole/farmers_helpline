package com.nath.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class AuditSendSmsMapper implements RowMapper<AuditSendSms> {

	@Override
	public AuditSendSms mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		AuditSendSms asm=new AuditSendSms();
		asm.setId(rs.getInt("id"));
		asm.setMobileNumber(rs.getString("mobile_numbers"));
		asm.setSms(rs.getString("sms"));
		asm.setUserID(rs.getString("user_id"));
		asm.setCreatedOn(rs.getTimestamp("created_on"));
		asm.setCreatedBy(rs.getString("created_by"));
		return asm;
	}

	
}
