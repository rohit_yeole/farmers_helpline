package com.nath.model;

public class DistrictStgModel {
	long id;
	String distCode;
	String distName;
	String stateCode;
	String stateName;
	String log;
	int requestId;
	
	
	
	
	public DistrictStgModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DistrictStgModel(String distCode, String distName, String stateCode, String stateName, String log,
			int requestId) {
		super();
		this.distCode = distCode;
		this.distName = distName;
		this.stateCode = stateCode;
		this.stateName = stateName;
		this.log = log;
		this.requestId = requestId;
	}

	public DistrictStgModel(String distCode, String distName, String stateCode, String stateName) {
		super();
		this.distCode = distCode;
		this.distName = distName;
		this.stateCode = stateCode;
		this.stateName = stateName;
	}
	
	
	public DistrictStgModel(long id, String distCode, String distName, String stateCode, String stateName, String log,
			int requestId) {
		super();
		this.id = id;
		this.distCode = distCode;
		this.distName = distName;
		this.stateCode = stateCode;
		this.stateName = stateName;
		this.log = log;
		this.requestId = requestId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDistCode() {
		return distCode;
	}
	public void setDistCode(String distCode) {
		this.distCode = distCode;
	}
	public String getDistName() {
		return distName;
	}
	public void setDistName(String distName) {
		this.distName = distName;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	@Override
	public String toString() {
		return "DistrictStgModel [distCode=" + distCode + ", distName=" + distName + ", stateCode=" + stateCode
				+ ", stateName=" + stateName + ", log=" + log + ", requestId=" + requestId + "]";
	}
	
	
	
}
