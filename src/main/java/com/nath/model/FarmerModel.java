package com.nath.model;

public class FarmerModel {

	String no;
	String mobileNo;
	String farmerName;
	String village;
	String taluka;
	String district;
	String state;
	String winBranch;
	String branch;
	String digit;
	int requestId;
	
	String log;
	public FarmerModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FarmerModel(String no, String mobileNo, String farmerName, String village, String taluka, String district,
			String state, String winBranch, String branch, String digit, int requestId, String log) {
		super();
		this.no = no;
		this.mobileNo = mobileNo;
		this.farmerName = farmerName;
		this.village = village;
		this.taluka = taluka;
		this.district = district;
		this.state = state;
		this.winBranch = winBranch;
		this.branch = branch;
		this.digit = digit;
		this.requestId = requestId;
		this.log = log;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getFarmerName() {
		return farmerName;
	}
	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getTaluka() {
		return taluka;
	}
	public void setTaluka(String taluka) {
		this.taluka = taluka;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getWinBranch() {
		return winBranch;
	}
	public void setWinBranch(String winBranch) {
		this.winBranch = winBranch;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getDigit() {
		return digit;
	}
	public void setDigit(String digit) {
		this.digit = digit;
	}
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	@Override
	public String toString() {
		return "FarmerModel [no=" + no + ", mobileNo=" + mobileNo + ", farmerName=" + farmerName + ", village="
				+ village + ", taluka=" + taluka + ", district=" + district + ", state=" + state + ", winBranch="
				+ winBranch + ", branch=" + branch + ", digit=" + digit + ", requestId=" + requestId + ", log=" + log
				+ "]";
	}
	
	
	}
