package com.nath.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class VarientStgModelMapper implements RowMapper<VarientStgModel> {

	@Override
	public VarientStgModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		VarientStgModel varientStgModel = new VarientStgModel();

		varientStgModel.setId(rs.getLong("id"));
		varientStgModel.setMobileNo(rs.getString("mobileNo"));
		varientStgModel.setCropCode(rs.getString("cropCode"));
		varientStgModel.setVarientCode(rs.getString("varientCode"));
		varientStgModel.setLog(rs.getString("log"));
		varientStgModel.setRequestId((rs.getInt("request_id")));

		return varientStgModel;
	}

}
