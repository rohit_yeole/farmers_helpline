package com.nath.model;

public class TalukaStgModel {

	private long id;
	private String talukaCode;
	private String talukaName;

	private String districtCode;
	private String districtName;
	private String log;
	private int requestId;

	public TalukaStgModel(long id, String talukaCode, String talukaName, String districtCode, String districtName,
			String log, int requestId) {
		super();
		this.id = id;
		this.talukaCode = talukaCode;
		this.talukaName = talukaName;
		this.districtCode = districtCode;
		this.districtName = districtName;
		this.log = log;
		this.requestId = requestId;
	}

	public TalukaStgModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTalukaCode() {
		return talukaCode;
	}

	public void setTalukaCode(String talukaCode) {
		this.talukaCode = talukaCode;
	}

	public String getTalukaName() {
		return talukaName;
	}

	public void setTalukaName(String talukaName) {
		this.talukaName = talukaName;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	@Override
	public String toString() {
		return "TalukaModel [id=" + id + ", talukaCode=" + talukaCode + ", talukaName=" + talukaName + ", districtCode="
				+ districtCode + ", districtName=" + districtName + ", log=" + log + ", requestId=" + requestId + "]";
	}

}
