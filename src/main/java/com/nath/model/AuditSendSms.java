package com.nath.model;

import java.sql.Timestamp;
import java.util.Date;

public class AuditSendSms {

	private int id;
	private String userID;
	private String mobileNumber;
	private String sms;
	private String createdBy;
	private Date createdOn;
	
	
	
	public AuditSendSms() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public AuditSendSms(int id, String userID, String mobileNumber, String sms, String createdBy, Date createdOn) {
		super();
		this.id = id;
		this.userID = userID;
		this.mobileNumber = mobileNumber;
		this.sms = sms;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	@Override
	public String toString() {
		return "AuditSendSms [id=" + id + ", userID=" + userID + ", mobileNumber=" + mobileNumber + ", sms=" + sms
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + "]";
	}
	
	
}
