package com.nath.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class FileUploadRequestMapper implements RowMapper<FileUploadRequest>{

	@Override
	public FileUploadRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		FileUploadRequest fur=new FileUploadRequest();
		// TODO Auto-generated method stub
		fur.setRequestId(rs.getInt("request_id"));
		fur.setUserId(rs.getInt("user_id"));
		fur.setFileName(rs.getString("request_date"));
		fur.setStatus(rs.getString("status"));
		fur.setRequestDate(rs.getDate("request_date"));
		return fur;
	}

}
