package com.nath.service;

import java.util.List;

import com.nath.dto.RoleAddDTO;
import com.nath.dto.RoleUpdateDTO;
import com.nath.dto.RolesDTO;

public interface RoleService {

	public List<RolesDTO> getRoles(int page, int limit) throws Exception;

	public RolesDTO getRoleById(int id) throws Exception;

	public String addRole(RoleAddDTO country) throws Exception;

	public boolean updateRole(RoleUpdateDTO country1) throws Exception;

	public boolean deleteRole(int id) throws Exception;

	public Long getRolesCount() throws Exception;

}
