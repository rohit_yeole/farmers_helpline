/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.CountryAddDTO;
import com.nath.dto.CountryDTO;
import com.nath.dto.CountryUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface CountryService {

	public List<CountryDTO> getCountries(int page, int limit) throws Exception;

	public CountryDTO getCountryById(String code) throws Exception;

	public String addCountry(CountryAddDTO country) throws Exception;

	public boolean updateCountry(CountryUpdateDTO country1) throws Exception;

	public boolean deleteCountry(String code) throws Exception;

	public Long getCountriesCount() throws Exception;
}
