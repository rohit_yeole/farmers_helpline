package com.nath.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.Branch;
import com.nath.bo.Farmer;
import com.nath.bo.FarmerVarientMapping;
import com.nath.bo.Taluka;
import com.nath.bo.Varient;
import com.nath.dao.FarmerDao;
import com.nath.dao.TalukaDao;
import com.nath.dto.FarmerAddDTO;
import com.nath.dto.FarmerDTO;
import com.nath.dto.FarmerSearchDTO;
import com.nath.dto.FarmerUpdateDTO;
import com.nath.dto.VarientDTO;
import com.nath.service.FarmerService;

@Service
@Transactional
public class FarmerServiceImpl implements FarmerService {

	@Autowired
	FarmerDao farmerDao;

	@Autowired
	TalukaDao talukaDao;

	@Override
	public List<FarmerDTO> getFarmers(int page, int limit) throws Exception {

		List<FarmerDTO> farmerDTOs = new ArrayList<>();
		List<Farmer> farmers;
		VarientDTO varientDTO = null;
		Set<VarientDTO> varientDTOs;
		try {

			farmers = farmerDao.getFarmers(page, limit);
			if (null != farmers && farmers.size() != 0) {
				for (Farmer farmer : farmers) {
					FarmerDTO dto = new FarmerDTO();
					varientDTOs = new HashSet<>();
					dto.setFarmerId(farmer.getFarmerId());
					dto.setName(farmer.getName());
					dto.setMobileNo(farmer.getMobileNo());
					dto.setVillageName(farmer.getVillageName());
					dto.setTalukaCode(farmer.getTaluka().getTalukaCode());
					dto.setTalukaName(farmer.getTaluka().getTalukaName());
					dto.setDistrictCode(farmer.getTaluka().getDistrict().getDistrictCode());
					dto.setDistrictName(farmer.getTaluka().getDistrict().getDistrictName());
					dto.setStateCode(farmer.getTaluka().getDistrict().getState().getStateCode());
					dto.setStateName(farmer.getTaluka().getDistrict().getState().getStateName());
					dto.setBranchCode(farmer.getBranch().getBranchCode());
					dto.setBranchName(farmer.getBranch().getBranchName());
					Set<FarmerVarientMapping> farmerVarientMappings = farmer.getFarmerVarientMappings();
					for (FarmerVarientMapping farmerVarientMapping : farmerVarientMappings) {
						varientDTO = new VarientDTO();
						varientDTO.setVarientCode(farmerVarientMapping.getVarient().getVarientCode());
						varientDTO.setVarientName(farmerVarientMapping.getVarient().getVarientName());
						varientDTO.setCropCode(farmerVarientMapping.getVarient().getCrop().getCropCode());
						varientDTO.setCropName(farmerVarientMapping.getVarient().getCrop().getCropName());
						varientDTOs.add(varientDTO);
					}
					dto.setVarients(varientDTOs);
					farmerDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return farmerDTOs;
	}

	@Override
	public FarmerDTO getFarmerById(Integer id) throws Exception {
		Farmer farmer = null;
		FarmerDTO dto = null;
		VarientDTO varientDTO = null;
		Set<VarientDTO> varientDTOs;
		try {
			farmer = farmerDao.getFarmerById(id);
			varientDTOs = new HashSet<>();
			if (null != farmer) {
				dto = new FarmerDTO();
				dto.setFarmerId(farmer.getFarmerId());
				dto.setName(farmer.getName());
				dto.setMobileNo(farmer.getMobileNo());
				dto.setVillageName(farmer.getVillageName());
				dto.setTalukaCode(farmer.getTaluka().getTalukaCode());
				dto.setTalukaName(farmer.getTaluka().getTalukaName());
				dto.setDistrictCode(farmer.getTaluka().getDistrict().getDistrictCode());
				dto.setDistrictName(farmer.getTaluka().getDistrict().getDistrictName());
				dto.setStateCode(farmer.getTaluka().getDistrict().getState().getStateCode());
				dto.setStateName(farmer.getTaluka().getDistrict().getState().getStateName());
				dto.setBranchCode(farmer.getBranch().getBranchCode());
				dto.setBranchName(farmer.getBranch().getBranchName());
				Set<FarmerVarientMapping> farmerVarientMappings = farmer.getFarmerVarientMappings();
				for (FarmerVarientMapping farmerVarientMapping : farmerVarientMappings) {
					varientDTO = new VarientDTO();
					varientDTO.setVarientCode(farmerVarientMapping.getVarient().getVarientCode());
					varientDTO.setVarientName(farmerVarientMapping.getVarient().getVarientName());
					varientDTO.setCropCode(farmerVarientMapping.getVarient().getCrop().getCropCode());
					varientDTO.setCropName(farmerVarientMapping.getVarient().getCrop().getCropName());
					varientDTOs.add(varientDTO);
				}
				dto.setVarients(varientDTOs);

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return dto;
	}

	@Override
	public Integer addFarmer(FarmerAddDTO farmerDTO) throws Exception {
		Integer id;
		Farmer farmer = new Farmer();
		Branch branch = new Branch();
		Taluka taluka = new Taluka();
		FarmerVarientMapping varientMapping;
		Set<FarmerVarientMapping> cropMappings = new HashSet<>();

		branch.setBranchCode(farmerDTO.getBranchCode());
		taluka.setTalukaCode(farmerDTO.getTalukaCode());
		farmer.setName(farmerDTO.getName());
		farmer.setMobileNo(farmerDTO.getMobileNo());
		farmer.setVillageName(farmerDTO.getVillageName());
		farmer.setBranch(branch);
		farmer.setTaluka(taluka);
		farmer.setCreatedBy(farmerDTO.getCreatedBy());
		farmer.setIsDeleted(false);
		for (String varientCode : farmerDTO.getVarients()) {
			varientMapping = new FarmerVarientMapping();
			Varient varient = new Varient();
			varient.setVarientCode(varientCode);
			varientMapping.setVarient(varient);
			varientMapping.setFarmer(farmer);
			cropMappings.add(varientMapping);

		}
		cropMappings.remove(null);
		farmer.setFarmerVarientMappings(cropMappings);
		try {
			id = farmerDao.addFarmer(farmer);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateFarmer(FarmerUpdateDTO farmerUpdateDTO) throws Exception {

		Farmer farmer = new Farmer();
		Branch branch = new Branch();
		Taluka taluka = new Taluka();
		FarmerVarientMapping cropMapping;
		Set<FarmerVarientMapping> cropMappings = new HashSet<>();

		branch.setBranchCode(farmerUpdateDTO.getBranchCode());
		taluka.setTalukaCode(farmerUpdateDTO.getTalukaCode());
		try {
			farmer = farmerDao.getFarmerById((farmerUpdateDTO.getFarmerId()));
			if (null != farmer) {
				farmer.setName(farmerUpdateDTO.getName());
				farmer.setMobileNo(farmerUpdateDTO.getMobileNo());
				farmer.setVillageName(farmerUpdateDTO.getVillageName());
				farmer.setBranch(branch);
				farmer.setTaluka(taluka);
				farmer.setModifiedBy(farmerUpdateDTO.getModifiedBy());
				farmer.getFarmerVarientMappings().clear();
				for (String varientCode : farmerUpdateDTO.getVarients()) {

					Varient varient = new Varient();
					cropMapping = new FarmerVarientMapping();
					varient.setVarientCode(varientCode);
					cropMapping.setVarient(varient);
					cropMapping.setFarmer(farmer);
					if (!cropMappings.contains(cropMapping))
						cropMappings.add(cropMapping);
					farmer.getFarmerVarientMappings().add(cropMapping);

				}
				farmer.getFarmerVarientMappings().remove(null);
				farmerDao.updateFarmer(farmer);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public boolean deleteFarmer(Integer id) throws Exception {
		Farmer farmer;
		try {
			farmer = farmerDao.getFarmerById(id);
			if (null != farmer) {
				farmer.setIsDeleted(true);
				farmerDao.updateFarmer(farmer);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public List<FarmerDTO> getFarmersByFilter(FarmerSearchDTO farmerSearchDTO) throws Exception {

		List<FarmerDTO> farmerDTOs = new ArrayList<>();
		List<Farmer> farmers;
		try {
			farmers = farmerDao.getFarmersByFilter(farmerSearchDTO);
			if (null != farmers && farmers.size() != 0) {
				for (Farmer farmer : farmers) {
					FarmerDTO dto = new FarmerDTO();
					dto.setFarmerId(farmer.getFarmerId());
					dto.setName(farmer.getName());
					dto.setMobileNo(farmer.getMobileNo());
					dto.setVillageName(farmer.getVillageName());
					dto.setTalukaCode(farmer.getTaluka().getTalukaCode());
					dto.setTalukaName(farmer.getTaluka().getTalukaName());
					dto.setDistrictCode(farmer.getTaluka().getDistrict().getDistrictCode());
					dto.setDistrictName(farmer.getTaluka().getDistrict().getDistrictName());
					dto.setStateCode(farmer.getTaluka().getDistrict().getState().getStateCode());
					dto.setStateName(farmer.getTaluka().getDistrict().getState().getStateName());
					dto.setBranchCode(farmer.getBranch().getBranchCode());
					dto.setBranchName(farmer.getBranch().getBranchName());
					farmerDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return farmerDTOs;
	}

	@Override
	public Long getFarmersCount() throws Exception {
		Long count;
		try {
			count = farmerDao.getFarmersCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}
}
