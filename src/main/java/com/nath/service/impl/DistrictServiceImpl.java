package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.District;
import com.nath.bo.State;
import com.nath.dao.CountryDao;
import com.nath.dao.DistrictDao;
import com.nath.dto.DistrictAddDTO;
import com.nath.dto.DistrictDTO;
import com.nath.dto.DistrictUpdateDTO;
import com.nath.service.DistrictService;

@Service
@Transactional
public class DistrictServiceImpl implements DistrictService {

	@Autowired
	DistrictDao districtDao;

	@Autowired
	CountryDao countryDao;

	@Override
	public List<DistrictDTO> getDistricts(int page, int limit) throws Exception {

		List<DistrictDTO> districtDTOs = new ArrayList<>();
		List<District> districts;
		try {
			int offset = (page - 1) * limit;
			districts = districtDao.getDistricts(offset, limit);
			if (null != districts || !districts.isEmpty()) {
				for (District district : districts) {
					DistrictDTO dto = new DistrictDTO();
					dto.setDistrictCode(district.getDistrictCode());
					dto.setDistrictName(district.getDistrictName());
					dto.setStateCode(district.getState().getStateCode());
					dto.setStateName(district.getState().getStateName());
					districtDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return districtDTOs;
	}

	@Override
	public List<DistrictDTO> getDistrictsByStateCode(String stateCode) throws Exception {

		List<DistrictDTO> districtDTOs = new ArrayList<>();
		List<District> districts;
		try {
			districts = districtDao.getDistrictsByStateCode(stateCode);
			if (null != districts || !districts.isEmpty()) {
				for (District district : districts) {
					DistrictDTO dto = new DistrictDTO();
					dto.setDistrictCode(district.getDistrictCode());
					dto.setDistrictName(district.getDistrictName());
					dto.setStateCode(district.getState().getStateCode());
					dto.setStateName(district.getState().getStateName());
					districtDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return districtDTOs;
	}

	@Override
	public DistrictDTO getDistrictByCode(String code) throws Exception {
		District district = null;
		DistrictDTO dto = null;
		try {
			district = districtDao.getDistrictByCode(code);
			if (null != district) {
				dto = new DistrictDTO();
				dto.setDistrictName(district.getDistrictName());
				dto.setDistrictCode(district.getDistrictCode());
				dto.setStateCode(district.getState().getStateCode());
				dto.setStateName(district.getState().getStateName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return dto;
	}

	@Override
	public String addDistrict(DistrictAddDTO districtAddDTO) throws Exception {
		String id;
		District district = new District();
		State state = new State();
		state.setStateCode(districtAddDTO.getStateCode());
		district.setDistrictName(districtAddDTO.getDistrictName());
		district.setDistrictCode(districtAddDTO.getDistrictCode());
		district.setState(state);
		district.setCreatedBy(districtAddDTO.getCreatedBy());
		district.setIsDeleted(false);
		try {
			id = districtDao.addDistrict(district);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateDistrict(DistrictUpdateDTO districtDTOUpdateDTO) throws Exception {
		District district;
		State state = new State();
		state.setStateCode(districtDTOUpdateDTO.getStateCode());
		try {
			district = districtDao.getDistrictByCode(districtDTOUpdateDTO.getDistrictCode());
			if (null != district) {
//				district.setDistrictId(districtDTOUpdateDTO.getDistrictId());
				district.setDistrictName(districtDTOUpdateDTO.getDistrictName());
				district.setDistrictCode(districtDTOUpdateDTO.getDistrictCode());
				district.setState(state);
				district.setModifiedBy(districtDTOUpdateDTO.getModifiedBy());
				districtDao.updateDistrict(district);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public boolean deleteDistrict(String code) throws Exception {
		District district;
		try {
			district = districtDao.getDistrictByCode(code);
			if (null != district) {
				district.setIsDeleted(true);
				districtDao.updateDistrict(district);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	
	@Override
	public Long getDistrictsCount() throws Exception {
		Long count;
		try {
			count = districtDao.getDistrictsCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
