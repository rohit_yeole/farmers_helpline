package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.Taluka;
import com.nath.bo.Village;
import com.nath.dao.TalukaDao;
import com.nath.dao.VillageDao;
import com.nath.dto.VillageAddDTO;
import com.nath.dto.VillageDTO;
import com.nath.dto.VillageUpdateDTO;
import com.nath.service.VillageService;

@Service
@Transactional
public class VillageServiceImpl implements VillageService {

	@Autowired
	VillageDao villageDao;

	@Autowired
	TalukaDao talukaDao;

	@Override
	public List<VillageDTO> getVillages(int page, int limit) throws Exception {

		List<VillageDTO> villageDTOs = new ArrayList<>();
		List<Village> villages;
		try {
			int offset = (page - 1) * limit;
			villages = villageDao.getVillages(offset, limit);
			if (null != villages || !villages.isEmpty()) {
				for (Village village : villages) {
					VillageDTO dto = new VillageDTO();
					dto.setVillageCode(village.getVillageCode());
					dto.setVillageName(village.getVillageName());
					dto.setTalukaCode(village.getTaluka().getTalukaCode());
					dto.setTalukaName(village.getTaluka().getTalukaName());
					villageDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return villageDTOs;
	}

	@Override
	public VillageDTO getVillageByCode(String code) throws Exception {
		Village village = null;
		VillageDTO dto = null;
		try {
			village = villageDao.getVillageByCode(code);
			if (null != village) {
				dto = new VillageDTO();
//				dto.setTaluk/saId(village.getVillageId());
				dto.setVillageCode(village.getVillageCode());
				dto.setVillageName(village.getVillageName());
				dto.setTalukaCode(village.getTaluka().getTalukaCode());
				dto.setTalukaName(village.getTaluka().getTalukaName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return dto;
	}

	@Override
	public String addVillage(VillageAddDTO villageDTO) throws Exception {
		String id;
		Village village = new Village();
		Taluka taluka = new Taluka();
		taluka.setTalukaCode(villageDTO.getTalukaCode());
		village.setVillageCode(villageDTO.getVillageCode());
		village.setVillageName(villageDTO.getVillageName());
		village.setTaluka(taluka);
		village.setCreatedBy(villageDTO.getCreatedBy());
		village.setIsDeleted(false);
		try {
			id = villageDao.addVillage(village);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateVillage(VillageUpdateDTO villageUpdateDTO) throws Exception {

		Village village = new Village();
		Taluka taluka = new Taluka();
		taluka.setTalukaCode(villageUpdateDTO.getTalukaCode());
		try {
			village = villageDao.getVillageByCode(villageUpdateDTO.getVillageCode());
			if (null != village) {
//				village.setVillageId(villageDTO.getVillageId());
				village.setVillageCode(villageUpdateDTO.getVillageCode());
				village.setVillageName(villageUpdateDTO.getVillageName());
				village.setTaluka(taluka);
				village.setModifiedBy(villageUpdateDTO.getModifiedBy());
				villageDao.updateVillage(village);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public boolean deleteVillage(String code) throws Exception {
		Village village;
		try {
			village = villageDao.getVillageByCode(code);
			if (null != village) {
				village.setIsDeleted(true);
				villageDao.updateVillage(village);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	
	@Override
	public Long getVillagesCount() throws Exception {
		Long count;
		try {
			count = villageDao.getVillagesCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}
}
