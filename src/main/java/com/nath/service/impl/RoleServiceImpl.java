/**
 * 
 */
package com.nath.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.MastRoleModules;
import com.nath.bo.MstModules;
import com.nath.bo.MstRoles;
import com.nath.dao.RoleDao;
import com.nath.dto.ModulesDTO;
import com.nath.dto.RoleAddDTO;
import com.nath.dto.RoleUpdateDTO;
import com.nath.dto.RolesDTO;
import com.nath.service.RoleService;

/**
 * @author Rohit
 *
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleDao roleDao;

	@Override
	public Long getRolesCount() throws Exception {
		Long count;
		try {
			count = roleDao.getRolesCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

	@Override
	public List<RolesDTO> getRoles(int page, int limit) throws Exception {
		List<RolesDTO> rolesDTOs = new ArrayList<RolesDTO>();
		List<MstRoles> roleList = null;
		try {
			int offset = (page - 1) * limit;
			roleList = roleDao.getRoles(offset, limit);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		if (null != roleList && roleList.size() != 0) {
			for (MstRoles role : roleList) {
				RolesDTO rolesDTO = new RolesDTO();
				rolesDTO.setRoleId(role.getRoleId());
				rolesDTO.setRoleName(role.getRoleName());
				rolesDTO.setRoleDesc(role.getRoleDesc());
				rolesDTO.setStatus(role.getStatus());
				Set<MastRoleModules> roleModules = role.getMastRoleModuleses();
				Set<ModulesDTO> modulesDTOs = new HashSet<>();
				for (MastRoleModules mastRoleModules : roleModules) {
					ModulesDTO modulesDTO = new ModulesDTO();
					modulesDTO.setModuleId(mastRoleModules.getMstModules().getModuleId());
					modulesDTO.setModuleName(mastRoleModules.getMstModules().getModuleName());
					modulesDTO.setModuleDesc(mastRoleModules.getMstModules().getModuleDesc());
					modulesDTO.setModuleGroup(mastRoleModules.getMstModules().getModuleGroup());
					modulesDTO.setModuleUrl(mastRoleModules.getMstModules().getModuleUrl());
					modulesDTOs.add(modulesDTO);
				}
				rolesDTO.setModulesDTO(modulesDTOs);
				rolesDTOs.add(rolesDTO);
			}
		}
		return rolesDTOs;
	}

	@Override
	public RolesDTO getRoleById(int id) throws Exception {
		RolesDTO rolesDTO = null;
		MstRoles role = null;
		try {
			role = roleDao.getRolesById(id);
			if (null != role) {
				rolesDTO = new RolesDTO();
				rolesDTO.setRoleId(role.getRoleId());
				rolesDTO.setRoleName(role.getRoleName());
				rolesDTO.setRoleDesc(role.getRoleDesc());
				rolesDTO.setStatus(role.getStatus());
				Set<MastRoleModules> roleModules = role.getMastRoleModuleses();
				Set<ModulesDTO> modulesDTOs = new HashSet<>();
				for (MastRoleModules mastRoleModules : roleModules) {
					ModulesDTO modulesDTO = new ModulesDTO();
					modulesDTO.setModuleId(mastRoleModules.getMstModules().getModuleId());
					modulesDTO.setModuleName(mastRoleModules.getMstModules().getModuleName());
					modulesDTO.setModuleDesc(mastRoleModules.getMstModules().getModuleDesc());
					modulesDTO.setModuleGroup(mastRoleModules.getMstModules().getModuleGroup());
					modulesDTO.setModuleUrl(mastRoleModules.getMstModules().getModuleUrl());
					modulesDTOs.add(modulesDTO);
				}
				rolesDTO.setModulesDTO(modulesDTOs);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return rolesDTO;
	}

	@Override
	public String addRole(RoleAddDTO roleAddDTO) throws Exception {
		String id;
		MstRoles role = new MstRoles();
		MastRoleModules mstRoleModule;
		Set<MastRoleModules> mastRoleModules = new HashSet<MastRoleModules>();

		role.setRoleName(roleAddDTO.getRoleName());
		role.setRoleDesc(roleAddDTO.getRoleDesc());
		role.setStatus(roleAddDTO.getStatus());
		role.setIsDeleted(false);
		for (Integer moduleId : roleAddDTO.getModuleDTOs()) {
			mstRoleModule = new MastRoleModules();
			MstModules module = new MstModules();
			module.setModuleId(moduleId);
			mstRoleModule.setMstModules(module);
			mstRoleModule.setMstRoles(role);
			mastRoleModules.add(mstRoleModule);
		}
		mastRoleModules.remove(null);
		role.setMastRoleModuleses(mastRoleModules);
		try {
			id = roleDao.addRoles(role);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateRole(RoleUpdateDTO roleUpdateDto) throws Exception {
		MstRoles role = null;
		MastRoleModules mstRoleModule;
		Set<MastRoleModules> mastRoleModules = new HashSet<>();
		try {
			role = roleDao.getRolesById(roleUpdateDto.getRoleId());
			if (null != role) {
				role.setRoleName(roleUpdateDto.getRoleName());
				role.setRoleDesc(roleUpdateDto.getRoleDesc());
				role.setStatus(roleUpdateDto.getStatus());
				role.getMastRoleModuleses().clear();
				for (Integer moduleId : roleUpdateDto.getModuleDTOs()) {

					MstModules mstModules = new MstModules();
					mstRoleModule = new MastRoleModules();
					mstModules.setModuleId(moduleId);
					mstRoleModule.setMstModules(mstModules);
					mstRoleModule.setMstRoles(role);
					if (!mastRoleModules.contains(mstRoleModule))
						mastRoleModules.add(mstRoleModule);
					role.getMastRoleModuleses().add(mstRoleModule);
				}
				role.getMastRoleModuleses().remove(null);
				roleDao.updateRoles(role);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public boolean deleteRole(int id) throws Exception {
		MstRoles role;
		try {
			role = roleDao.getRolesById(id);
			if (null != role) {
				role.setIsDeleted(true);
				roleDao.updateRoles(role);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

}
