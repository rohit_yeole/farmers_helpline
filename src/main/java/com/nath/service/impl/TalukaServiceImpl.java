package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.District;
import com.nath.bo.Taluka;
import com.nath.dao.DistrictDao;
import com.nath.dao.TalukaDao;
import com.nath.dto.TalukaAddDTO;
import com.nath.dto.TalukaDTO;
import com.nath.dto.TalukaUpdateDTO;
import com.nath.service.TalukaService;

@Service
@Transactional
public class TalukaServiceImpl implements TalukaService {

	@Autowired
	TalukaDao talukaDao;

	@Autowired
	DistrictDao districtDao;

	@Override
	public List<TalukaDTO> getTalukas(int page, int limit) throws Exception {

		List<TalukaDTO> talukaDTOs = new ArrayList<>();
		List<Taluka> talukas;
		try {
			int offset = (page - 1) * limit;
			talukas = talukaDao.getTalukas(offset, limit);
			if (null != talukas || !talukas.isEmpty()) {
				for (Taluka taluka : talukas) {
					TalukaDTO dto = new TalukaDTO();
//					dto.setTalukaCode(taluka.getTalukaId());
					dto.setTalukaCode(taluka.getTalukaCode());
					dto.setTalukaName(taluka.getTalukaName());
					dto.setDistrictCode(taluka.getDistrict().getDistrictCode());
					dto.setDistrictName(taluka.getDistrict().getDistrictName());
					talukaDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return talukaDTOs;
	}

	@Override
	public List<TalukaDTO> getTalukasByDistrictCode(String districtCode) throws Exception {

		List<TalukaDTO> talukaDTOs = new ArrayList<>();
		List<Taluka> talukas;
		try {
			talukas = talukaDao.getTalukasByDistrictCode(districtCode);
			if (null != talukas || !talukas.isEmpty()) {
				for (Taluka taluka : talukas) {
					TalukaDTO dto = new TalukaDTO();
//					dto.setTalukaCode(taluka.getTalukaId());
					dto.setTalukaCode(taluka.getTalukaCode());
					dto.setTalukaName(taluka.getTalukaName());
					dto.setDistrictCode(taluka.getDistrict().getDistrictCode());
					dto.setDistrictName(taluka.getDistrict().getDistrictName());
					talukaDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return talukaDTOs;
	}

	@Override
	public TalukaDTO getTalukaByCode(String code) throws Exception {
		Taluka taluka = null;
		TalukaDTO dto = null;
		try {
			taluka = talukaDao.getTalukaByCode(code);
			if (null != taluka) {
				dto = new TalukaDTO();
//				dto.setTaluk/saId(taluka.getTalukaId());
				dto.setTalukaCode(taluka.getTalukaCode());
				dto.setTalukaName(taluka.getTalukaName());
				dto.setDistrictCode(taluka.getDistrict().getDistrictCode());
				dto.setDistrictName(taluka.getDistrict().getDistrictName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return dto;
	}

	@Override
	public String addTaluka(TalukaAddDTO talukaDTO) throws Exception {
		String id;
		Taluka taluka = new Taluka();
		District district = new District();
		district.setDistrictCode(talukaDTO.getDistrictCode());
		taluka.setTalukaCode(talukaDTO.getTalukaCode());
		taluka.setTalukaName(talukaDTO.getTalukaName());
		taluka.setDistrict(district);
		taluka.setCreatedBy(talukaDTO.getCreatedBy());
		taluka.setIsDeleted(false);
		try {
			id = talukaDao.addTaluka(taluka);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateTaluka(TalukaUpdateDTO talukaUpdateDTO) throws Exception {

		Taluka taluka = new Taluka();
		District district = new District();
		district.setDistrictCode(talukaUpdateDTO.getDistrictCode());
		try {
			taluka = talukaDao.getTalukaByCode(talukaUpdateDTO.getTalukaCode());
			if (null != taluka) {
//				taluka.setTalukaId(talukaDTO.getTalukaId());
				taluka.setTalukaCode(talukaUpdateDTO.getTalukaCode());
				taluka.setTalukaName(talukaUpdateDTO.getTalukaName());
				taluka.setDistrict(district);
				taluka.setModifiedBy(talukaUpdateDTO.getModifiedBy());
				talukaDao.updateTaluka(taluka);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public boolean deleteTaluka(String code) throws Exception {
		Taluka taluka;
		try {
			taluka = talukaDao.getTalukaByCode(code);
			if (null != taluka) {
				taluka.setIsDeleted(true);
				talukaDao.updateTaluka(taluka);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	
	@Override
	public Long getTalukasCount() throws Exception {
		Long count;
		try {
			count = talukaDao.getTalukasCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
