/**
 * 
 */
package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.MstModules;
import com.nath.dao.ModuleDao;
import com.nath.dto.ModuleAddDTO;
import com.nath.dto.ModuleUpdateDTO;
import com.nath.dto.ModulesDTO;
import com.nath.service.ModuleService;

/**
 * @author Rohit
 *
 */
@Service
@Transactional
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	ModuleDao moduleDao;

	@Override
	public Long getModulesCount() throws Exception {
		Long count;
		try {
			count = moduleDao.getModulesCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

	@Override
	public List<ModulesDTO> getModules(int page, int limit) throws Exception {
		List<ModulesDTO> userDTOs = new ArrayList<ModulesDTO>();
		List<MstModules> userList = null;
		try {
			int offset = (page - 1) * limit;
			userList = moduleDao.getModules(offset, limit);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		if (null != userList && userList.size() != 0) {
			for (MstModules user : userList) {
				ModulesDTO userDTO = new ModulesDTO();
				userDTO.setModuleId(user.getModuleId());
				userDTO.setModuleName(user.getModuleName());
				userDTO.setModuleDesc(user.getModuleDesc());
				userDTO.setModuleUrl(user.getModuleUrl());
				userDTO.setModuleGroup(user.getModuleGroup());
				userDTO.setStatus(user.getStatus());
				userDTOs.add(userDTO);
			}
		}
		return userDTOs;
	}

	@Override
	public ModulesDTO getModuleById(int id) throws Exception {
		ModulesDTO userDTO = null;
		MstModules user = null;
		try {
			user = moduleDao.getModulesById(id);
			if (null != user) {
				userDTO = new ModulesDTO();
				userDTO.setModuleId(user.getModuleId());
				userDTO.setModuleName(user.getModuleName());
				userDTO.setModuleDesc(user.getModuleDesc());
				userDTO.setModuleUrl(user.getModuleUrl());
				userDTO.setModuleGroup(user.getModuleGroup());
				userDTO.setStatus(user.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return userDTO;
	}

	@Override
	public String addModule(ModuleAddDTO userAddDTO) throws Exception {
		String id;
		MstModules module = new MstModules();
		module.setModuleName(userAddDTO.getModuleName());
		module.setModuleDesc(userAddDTO.getModuleDesc());
		module.setModuleUrl(userAddDTO.getModuleUrl());
		module.setModuleGroup(userAddDTO.getModuleGroup());
		module.setStatus(userAddDTO.getStatus());
		module.setIsDeleted(false);
		try {
			id = moduleDao.addModules(module);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateModule(ModuleUpdateDTO userDto) throws Exception {
		MstModules module = null;
		try {
			module = moduleDao.getModulesById(userDto.getModuleId());
			if (null != module) {
				module.setModuleName(userDto.getModuleName());
				module.setModuleDesc(userDto.getModuleDesc());
				module.setModuleUrl(userDto.getModuleUrl());
				module.setModuleGroup(userDto.getModuleGroup());
				module.setStatus(userDto.getStatus());
				moduleDao.updateModules(module);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public boolean deleteModule(int id) throws Exception {
		MstModules module;
		try {
			module = moduleDao.getModulesById(id);
			if (null != module) {
				module.setIsDeleted(true);
				moduleDao.updateModules(module);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

}
