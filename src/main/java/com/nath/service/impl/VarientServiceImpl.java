package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.Crop;
import com.nath.bo.Varient;
import com.nath.dao.CropDao;
import com.nath.dao.VarientDao;
import com.nath.dto.VarientAddDTO;
import com.nath.dto.VarientDTO;
import com.nath.dto.VarientUpdateDTO;
import com.nath.service.VarientService;

@Service
@Transactional
public class VarientServiceImpl implements VarientService {

	@Autowired
	VarientDao varientDao;

	@Autowired
	CropDao cropDao;

	@Override
	public List<VarientDTO> getVarients(int page, int limit) throws Exception {

		List<Varient> varients = null;
		List<VarientDTO> varientDTOs = new ArrayList<>();

		try {
			int offset = (page - 1) * limit;
			varients = varientDao.getVarients(offset, limit);
			if (null != varients || !varients.isEmpty()) {
				for (Varient varient : varients) {
					VarientDTO dto = new VarientDTO();
					dto.setVarientCode(varient.getVarientCode());
					dto.setVarientName(varient.getVarientName());
					dto.setCropCode(varient.getCrop().getCropCode());
					dto.setCropName(varient.getCrop().getCropName());
					varientDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return varientDTOs;
	}

	@Override
	public VarientDTO getVarientByCode(String code) throws Exception {
		Varient varient = null;
		VarientDTO dto = null;
		try {
			varient = varientDao.getVarientByCode(code);
			if (null != varient) {
				dto = new VarientDTO();
				dto.setVarientName(varient.getVarientName());
				dto.setVarientCode(varient.getVarientCode());
				dto.setCropCode(varient.getCrop().getCropCode());
				dto.setCropName(varient.getCrop().getCropName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return dto;
	}

	@Override
	public String addVarient(VarientAddDTO varientDTO) throws Exception {
		String id;
		Crop crop = new Crop();
		Varient varient = new Varient();
		varient.setVarientCode(varientDTO.getVarientCode());
		varient.setVarientName(varientDTO.getVarientName());
		crop.setCropCode(varientDTO.getCropCode());
		varient.setCrop(crop);
		varient.setCreatedBy(varientDTO.getCreatedBy());
		varient.setIsDeleted(false);
		try {
			id = varientDao.addVarient(varient);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateVarient(VarientUpdateDTO varientDTO) throws Exception {
		Varient varient;
		Crop crop = new Crop();
		crop.setCropCode(varientDTO.getCropCode());

		try {
			varient = varientDao.getVarientByCode(varientDTO.getVarientCode());
			if (null != varient) {
				varient.setVarientCode(varientDTO.getVarientCode());
				varient.setVarientName(varientDTO.getVarientName());
				varient.setCrop(crop);
				varient.setModifiedBy(varientDTO.getModifiedBy());
				varientDao.updateVarient(varient);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public boolean deleteVarient(String code) throws Exception {
		Varient varient;
		try {
			varient = varientDao.getVarientByCode(code);
			if (null != varient) {
				varient.setIsDeleted(true);
				varientDao.updateVarient(varient);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public List<VarientDTO> getVarientsByCropCode(String cropCode) throws Exception {
		List<Varient> varients = null;
		List<VarientDTO> varientDTOs = new ArrayList<>();

		try {
			varients = varientDao.getVarientsByCropCode(cropCode);
			if (null != varients || !varients.isEmpty()) {
				for (Varient varient : varients) {
					VarientDTO dto = new VarientDTO();
					dto.setVarientCode(varient.getVarientCode());
					dto.setVarientName(varient.getVarientName());
					dto.setCropCode(varient.getCrop().getCropCode());
					dto.setCropName(varient.getCrop().getCropName());
					varientDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return varientDTOs;
	}
	
	@Override
	public Long getVarientsCount() throws Exception {
		Long count;
		try {
			count = varientDao.getVarientsCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
