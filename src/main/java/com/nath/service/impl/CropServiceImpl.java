/**
 * 
 */
package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.Crop;
import com.nath.dao.CropDao;
import com.nath.dto.CropAddDTO;
import com.nath.dto.CropDTO;
import com.nath.dto.CropUpdateDTO;
import com.nath.service.CropService;

/**
 * @author Rohit
 *
 */
@Service
@Transactional
public class CropServiceImpl implements CropService {

	@Autowired
	CropDao cropDao;

	@Override
	public List<CropDTO> getCrops(int page, int limit) throws Exception {
		List<CropDTO> cropDTOs = new ArrayList<CropDTO>();
		List<Crop> cropList = null;
		try {
			int offset = (page - 1) * limit;
			cropList = cropDao.getCrops(offset, limit);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		if (null != cropList || !cropList.isEmpty()) {
			for (Crop crop : cropList) {
				CropDTO cropDTO = new CropDTO();
//				cropDTO.setCropId(crop.getCropId());
				cropDTO.setCropCode(crop.getCropCode());
				cropDTO.setCropName(crop.getCropName());
				cropDTOs.add(cropDTO);
			}
		}
		return cropDTOs;
	}

	@Override
	public CropDTO getCropByCode(String code) throws Exception {
		CropDTO cropDTO = null;
		Crop crop = null;
		try {
			crop = cropDao.getCropById(code);
			if (null != crop) {
				cropDTO = new CropDTO();
//				cropDTO.setCropId(crop.getCropId());
				cropDTO.setCropCode(crop.getCropCode());
				cropDTO.setCropName(crop.getCropName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return cropDTO;
	}

	@Override
	public String addCrop(CropAddDTO cropAddDTO) throws Exception {
		String id;
		Crop crop = new Crop();
		crop.setCropCode(cropAddDTO.getCropCode());
		crop.setCropName(cropAddDTO.getCropName());
		crop.setCreatedBy(cropAddDTO.getCreatedBy());
		crop.setIsDeleted(false);
		try {
			id = cropDao.addCrop(crop);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateCrop(CropUpdateDTO cropDto) throws Exception {
		Crop crop = null;
		try {
			crop = cropDao.getCropById(cropDto.getCropCode());
			if (null != crop) {
//				crop.setCropId(cropDto.getCropId());
				crop.setCropCode(cropDto.getCropCode());
				crop.setCropName(cropDto.getCropName());
				crop.setModifiedBy(cropDto.getModifiedBy());
				crop.setIsDeleted(false);
				cropDao.updateCrop(crop);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public boolean deleteCrop(String code) throws Exception {
		Crop crop;
		try {
			crop = cropDao.getCropById(code);
			if (null != crop) {
				crop.setIsDeleted(true);
				cropDao.updateCrop(crop);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	
	@Override
	public Long getCropsCount() throws Exception {
		Long count;
		try {
			count = cropDao.getCropsCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
