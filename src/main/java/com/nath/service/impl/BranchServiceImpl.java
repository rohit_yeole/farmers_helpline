package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.Branch;
import com.nath.dao.BranchDao;
import com.nath.dto.BranchAddDTO;
import com.nath.dto.BranchDTO;
import com.nath.dto.BranchUpdateDTO;
import com.nath.service.BranchService;

@Service
@Transactional
public class BranchServiceImpl implements BranchService {

	@Autowired
	BranchDao branchDao;

	@Override
	public List<BranchDTO> getBranches(int page, int limit) throws Exception {

		List<BranchDTO> branchDTOs = new ArrayList<>();
		List<Branch> branchs;
		try {
			int offset = (page - 1) * limit;
			branchs = branchDao.getBranches(offset, limit);
			if (null != branchs || !branchs.isEmpty()) {
				for (Branch branch : branchs) {
					BranchDTO dto = new BranchDTO();
					dto.setBranchCode(branch.getBranchCode());
					dto.setBranchName(branch.getBranchName());
					dto.setStatus(branch.getStatus());
					branchDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return branchDTOs;
	}

	@Override
	public BranchDTO getBranchByCode(String code) throws Exception {
		Branch branch = null;
		BranchDTO dto = null;
		try {
			branch = branchDao.getBranchByCode(code);
			if (null != branch) {
				dto = new BranchDTO();
				dto.setBranchCode(branch.getBranchCode());
				dto.setBranchName(branch.getBranchName());
				dto.setStatus(branch.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return dto;
	}

	@Override
	public String addBranch(BranchAddDTO branchDTO) throws Exception {
		String id;
		Branch branch = new Branch();
		branch.setBranchCode(branchDTO.getBranchCode());
		branch.setBranchName(branchDTO.getBranchName());
		branch.setStatus(branchDTO.getStatus());
		branch.setCreatedBy(branchDTO.getCreatedBy());
		branch.setIsDeleted(false);
		try {
			id = branchDao.addBranch(branch);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateBranch(BranchUpdateDTO branchUpdateDTO) throws Exception {

		Branch branch = new Branch();
		try {
			branch = branchDao.getBranchByCode(branchUpdateDTO.getBranchCode());
			if (null != branch) {
				branch.setBranchCode(branchUpdateDTO.getBranchCode());
				branch.setBranchName(branchUpdateDTO.getBranchName());
				branch.setStatus(branchUpdateDTO.getStatus());
				branch.setModifiedBy(branchUpdateDTO.getModifiedBy());
				branchDao.updateBranch(branch);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public boolean deleteBranch(String code) throws Exception {
		Branch branch;
		try {
			branch = branchDao.getBranchByCode(code);
			if (null != branch) {
				branch.setIsDeleted(true);
				branchDao.updateBranch(branch);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public Long getBranchesCount() throws Exception {
		Long count;
		try {
			count = branchDao.getBranchesCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}
}
