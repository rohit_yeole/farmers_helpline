/**
 * 
 */
package com.nath.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.MastRoleModules;
import com.nath.bo.MstRoles;
import com.nath.bo.MstUserRoles;
import com.nath.bo.MstUsers;
import com.nath.dao.UserDao;
import com.nath.dto.ModulesDTO;
import com.nath.dto.RolesDTO;
import com.nath.dto.UserAddDTO;
import com.nath.dto.UserDTO;
import com.nath.dto.UserUpdateDTO;
import com.nath.dto.UserValidateDTO;
import com.nath.dto.UserValidateResponseDTO;
import com.nath.service.UserService;

/**
 * @author Rohit
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;

	@Override
	public UserValidateResponseDTO authenticateUser(UserValidateDTO validateDTO) throws Exception {
		MstUsers mstUsers;
		UserValidateResponseDTO validateResponseDTO = null;
		Set<MstUserRoles> mstUserRoles = null;
		RolesDTO rolesDTO = null;
		Set<MastRoleModules> roleModules = null;
		ModulesDTO modulesDTO = null;
		Set<ModulesDTO> modulesDTOs = null;
		Set<RolesDTO> rolesDTOs = null;
		try {
			mstUsers = userDao.authenticateUser(validateDTO);
			if (null != mstUsers) {
				rolesDTOs = new HashSet<RolesDTO>();
				validateResponseDTO = new UserValidateResponseDTO();
				validateResponseDTO.setUsername(mstUsers.getUserName());
				validateResponseDTO.setEmail(mstUsers.getEmail());
				validateResponseDTO.setUserType(mstUsers.getUserType());
				mstUserRoles = mstUsers.getMstUserRoleses();
				for (MstUserRoles userRoles : mstUserRoles) {
					rolesDTO = new RolesDTO();
					modulesDTOs = new HashSet<ModulesDTO>();
					rolesDTO.setRoleName(userRoles.getMstRoles().getRoleName());
					rolesDTO.setRoleDesc(userRoles.getMstRoles().getRoleDesc());
					roleModules = userRoles.getMstRoles().getMastRoleModuleses();
					for (MastRoleModules mastRoleModules : roleModules) {
						modulesDTO = new ModulesDTO();
						modulesDTO.setModuleName(mastRoleModules.getMstModules().getModuleName());
						modulesDTO.setModuleDesc(mastRoleModules.getMstModules().getModuleDesc());
						modulesDTO.setModuleGroup(mastRoleModules.getMstModules().getModuleGroup());
						modulesDTO.setModuleUrl(mastRoleModules.getMstModules().getModuleUrl());
						modulesDTOs.add(modulesDTO);
					}
					rolesDTO.setModulesDTO(modulesDTOs);
					rolesDTOs.add(rolesDTO);
				}
				validateResponseDTO.setRolesDTO(rolesDTOs);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return validateResponseDTO;
	}

	@Override
	public Long getUsersCount() throws Exception {
		Long count;
		try {
			count = userDao.getUsersCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

	@Override
	public List<UserDTO> getUsers(int page, int limit) throws Exception {
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		List<MstUsers> userList = null;
		try {
			int offset = (page - 1) * limit;
			userList = userDao.getUsers(offset, limit);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		if (null != userList && userList.size() != 0) {
			for (MstUsers user : userList) {
				UserDTO userDTO = new UserDTO();
				userDTO.setUserId(user.getUserId());
				userDTO.setUserName(user.getUserName());
				userDTO.setEmail(user.getEmail());
				userDTO.setUserType(user.getUserType());
				Set<MstUserRoles> userRoles = user.getMstUserRoleses();
				Set<RolesDTO> rolesDTOs = new HashSet<>();
				for (MstUserRoles mstUserRoles : userRoles) {
					RolesDTO rolesDTO = new RolesDTO();
					rolesDTO.setRoleId(mstUserRoles.getMstRoles().getRoleId());
					rolesDTO.setRoleName(mstUserRoles.getMstRoles().getRoleName());
					rolesDTO.setRoleDesc(mstUserRoles.getMstRoles().getRoleDesc());
					rolesDTO.setStatus(mstUserRoles.getMstRoles().getStatus());
					Set<MastRoleModules> roleModules = mstUserRoles.getMstRoles().getMastRoleModuleses();
					Set<ModulesDTO> modulesDTOs = new HashSet<>();
					for (MastRoleModules mastRoleModules : roleModules) {
						ModulesDTO modulesDTO = new ModulesDTO();
						modulesDTO.setModuleId(mastRoleModules.getMstModules().getModuleId());
						modulesDTO.setModuleName(mastRoleModules.getMstModules().getModuleName());
						modulesDTO.setModuleDesc(mastRoleModules.getMstModules().getModuleDesc());
						modulesDTO.setModuleGroup(mastRoleModules.getMstModules().getModuleGroup());
						modulesDTO.setModuleUrl(mastRoleModules.getMstModules().getModuleUrl());
						modulesDTOs.add(modulesDTO);
					}
					rolesDTO.setModulesDTO(modulesDTOs);
					rolesDTOs.add(rolesDTO);
				}
				userDTO.setRolesDTO(rolesDTOs);
				userDTOs.add(userDTO);
			}
		}
		return userDTOs;
	}

	@Override
	public UserDTO getUserById(int id) throws Exception {
		UserDTO userDTO = null;
		MstUsers user = null;
		try {
			user = userDao.getUserById(id);
			if (null != user) {
				userDTO = new UserDTO();
				userDTO.setUserId(user.getUserId());
				userDTO.setUserName(user.getUserName());
				userDTO.setEmail(user.getEmail());
				userDTO.setUserType(user.getUserType());
				Set<MstUserRoles> userRoles = user.getMstUserRoleses();
				Set<RolesDTO> rolesDTOs = new HashSet<>();
				for (MstUserRoles mstUserRoles : userRoles) {
					RolesDTO rolesDTO = new RolesDTO();
					rolesDTO.setRoleId(mstUserRoles.getMstRoles().getRoleId());
					rolesDTO.setRoleName(mstUserRoles.getMstRoles().getRoleName());
					rolesDTO.setRoleDesc(mstUserRoles.getMstRoles().getRoleDesc());
					rolesDTO.setStatus(mstUserRoles.getMstRoles().getStatus());
					Set<MastRoleModules> roleModules = mstUserRoles.getMstRoles().getMastRoleModuleses();
					Set<ModulesDTO> modulesDTOs = new HashSet<>();
					for (MastRoleModules mastRoleModules : roleModules) {
						ModulesDTO modulesDTO = new ModulesDTO();
						modulesDTO.setModuleId(mastRoleModules.getMstModules().getModuleId());
						modulesDTO.setModuleName(mastRoleModules.getMstModules().getModuleName());
						modulesDTO.setModuleDesc(mastRoleModules.getMstModules().getModuleDesc());
						modulesDTO.setModuleGroup(mastRoleModules.getMstModules().getModuleGroup());
						modulesDTO.setModuleUrl(mastRoleModules.getMstModules().getModuleUrl());
						modulesDTOs.add(modulesDTO);
					}
					rolesDTO.setModulesDTO(modulesDTOs);
					rolesDTOs.add(rolesDTO);
				}
				userDTO.setRolesDTO(rolesDTOs);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return userDTO;
	}

	@Override
	public Integer addUser(UserAddDTO userAddDTO) throws Exception {
		Integer id = null;
		MstUsers user = new MstUsers();
		MstUserRoles mstUserRole;
		Set<MstUserRoles> mstUserRoles = new HashSet<MstUserRoles>();
		try {
			if (validateUsername(userAddDTO.getUserName())) {
				user.setUserName(userAddDTO.getUserName());
				user.setPassword(userAddDTO.getPassword());
				user.setEmail(userAddDTO.getEmail());
				user.setUserType(userAddDTO.getUserType());
				user.setIsDeleted(false);
				for (Integer roleId : userAddDTO.getRoleDTOs()) {
					mstUserRole = new MstUserRoles();
					MstRoles role = new MstRoles();
					role.setRoleId(roleId);
					mstUserRole.setMstRoles(role);
					mstUserRole.setMstUsers(user);
					mstUserRoles.add(mstUserRole);
				}
				mstUserRoles.remove(null);
				user.setMstUserRoleses(mstUserRoles);
				try {
					id = userDao.addUsers(user);
				} catch (Exception e) {
					e.printStackTrace();
					throw new Exception(e);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateUser(UserUpdateDTO userDto) throws Exception {
		MstUsers user = null;
		MstUserRoles mstUserRole;
		Set<MstUserRoles> mstUserRoles = new HashSet<MstUserRoles>();
		try {
			user = userDao.getUserById(userDto.getUserId());
			if (null != user) {
				user.setUserName(userDto.getUserName());
				user.setEmail(userDto.getEmail());
				if (null != userDto.getPassword() && userDto.getPassword().isEmpty())
					user.setPassword(userDto.getPassword());
				user.setUserType(userDto.getUserType());
				user.getMstUserRoleses().clear();
				for (Integer roleId : userDto.getRoleDTOs()) {

					MstRoles mstRole = new MstRoles();
					mstUserRole = new MstUserRoles();
					mstRole.setRoleId(roleId);
					mstUserRole.setMstRoles(mstRole);
					mstUserRole.setMstUsers(user);
					if (!mstUserRoles.contains(mstUserRole))
						mstUserRoles.add(mstUserRole);
					user.getMstUserRoleses().add(mstUserRole);
				}
				user.getMstUserRoleses().remove(null);
				userDao.updateUsers(user);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public boolean deleteUser(int id) throws Exception {
		MstUsers user;
		try {
			user = userDao.getUserById(id);
			if (null != user) {
				user.setIsDeleted(true);
				userDao.updateUsers(user);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public Boolean validateUsername(String username) throws Exception {
		MstUsers user = null;
		Boolean isValid = true;
		try {
			user = userDao.validateUsername(username);
			if (null != user) {
				isValid = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return isValid;

	}

}
