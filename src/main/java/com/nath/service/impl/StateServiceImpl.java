package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.Country;
import com.nath.bo.State;
import com.nath.dao.CountryDao;
import com.nath.dao.StateDao;
import com.nath.dto.StateAddDTO;
import com.nath.dto.StateDTO;
import com.nath.dto.StateUpdateDTO;
import com.nath.service.StateService;

@Service
@Transactional
public class StateServiceImpl implements StateService {

	@Autowired
	StateDao stateDao;

	@Autowired
	CountryDao countryDao;

	@Override
	public List<StateDTO> getStates(int page, int limit) throws Exception {

		List<State> states = null;
		List<StateDTO> stateDTOs = new ArrayList<>();

		try {
			int offset = (page - 1) * limit;
			states = stateDao.getStates(offset, limit);
			if (null != states || !states.isEmpty()) {
				for (State state : states) {
					StateDTO dto = new StateDTO();
//					dto.setStateId(state.getStateId());
					dto.setStateCode(state.getStateCode());
					dto.setStateName(state.getStateName());
					dto.setCountryCode(state.getCountry().getCountryCode());
					dto.setCountryName(state.getCountry().getCountryName());
					stateDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return stateDTOs;
	}

	@Override
	public StateDTO getStateByCode(String code) throws Exception {
		State state = null;
		StateDTO dto = null;
		try {
			state = stateDao.getStateByCode(code);
			if (null != state) {
				dto = new StateDTO();
//				dto.setStateId(state.getStateId());
				dto.setStateName(state.getStateName());
				dto.setStateCode(state.getStateCode());
				dto.setCountryCode(state.getCountry().getCountryCode());
				dto.setCountryName(state.getCountry().getCountryName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

		return dto;
	}

	@Override
	public String addState(StateAddDTO stateDTO) throws Exception {
		String id;
		State state = new State();
		Country country = new Country();
		country.setCountryCode(stateDTO.getCountrycode());
		state.setStateName(stateDTO.getStateName());
		state.setStateCode(stateDTO.getStateCode());
		state.setCountry(country);
		state.setCreatedBy(stateDTO.getCreatedBy());
		state.setIsDeleted(false);
		try {
			id = stateDao.addState(state);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateState(StateUpdateDTO stateDTO) throws Exception {
		State state;
		Country country = new Country();
		country.setCountryCode(stateDTO.getCountryCode());

		try {
			state = stateDao.getStateByCode(stateDTO.getStateCode());
			if (null != state) {
//				state.setStateId(stateDTO.getStateId());
				state.setStateCode(stateDTO.getStateCode());
				state.setStateName(stateDTO.getStateName());
				state.setCountry(country);
				state.setModifiedBy(stateDTO.getModifiedBy());
				stateDao.updateState(state);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public boolean deleteState(String code) throws Exception {
		State state;
		try {
			state = stateDao.getStateByCode(code);
			if (null != state) {
				state.setIsDeleted(true);
				stateDao.updateState(state);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Override
	public List<StateDTO> getStatesByCountryCode(String countryCode) throws Exception {
		List<State> states = null;
		List<StateDTO> stateDTOs = new ArrayList<>();

		try {
			states = stateDao.getStatesByCountryCode(countryCode);
			if (null != states || !states.isEmpty()) {
				for (State state : states) {
					StateDTO dto = new StateDTO();
					dto.setStateCode(state.getStateCode());
					dto.setStateName(state.getStateName());
					dto.setCountryCode(state.getCountry().getCountryCode());
					dto.setCountryName(state.getCountry().getCountryName());
					stateDTOs.add(dto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return stateDTOs;
	}

	@Override
	public Long getStatesCount() throws Exception {
		Long count;
		try {
			count = stateDao.getStatesCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
