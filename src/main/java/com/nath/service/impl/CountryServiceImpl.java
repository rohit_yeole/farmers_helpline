/**
 * 
 */
package com.nath.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nath.bo.Country;
import com.nath.dao.CountryDao;
import com.nath.dto.CountryAddDTO;
import com.nath.dto.CountryDTO;
import com.nath.dto.CountryUpdateDTO;
import com.nath.service.CountryService;

/**
 * @author Rohit
 *
 */
@Service
@Transactional
public class CountryServiceImpl implements CountryService {

	@Autowired
	CountryDao countryDao;

	@Override
	public List<CountryDTO> getCountries(int page, int limit) throws Exception {
		List<CountryDTO> countryDTOs = new ArrayList<CountryDTO>();
		List<Country> countryList = null;
		try {
			int offset = (page - 1) * limit;
			countryList = countryDao.getCountries(offset, limit);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		if (null != countryList || !countryList.isEmpty()) {
			for (Country country : countryList) {
				CountryDTO countryDTO = new CountryDTO();
				countryDTO.setCountryCode(country.getCountryCode());
				countryDTO.setCountryName(country.getCountryName());
				countryDTOs.add(countryDTO);
			}
		}
		return countryDTOs;
	}

	@Override
	public CountryDTO getCountryById(String code) throws Exception {
		CountryDTO countryDTO = null;
		Country country = null;
		try {
			country = countryDao.getCountryById(code);
			if (null != country) {
				countryDTO = new CountryDTO();
//				countryDTO.setCountryId(country.getCountryId());
				countryDTO.setCountryCode(country.getCountryCode());
				countryDTO.setCountryName(country.getCountryName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return countryDTO;
	}

	@Override
	public String addCountry(CountryAddDTO countryAddDTO) throws Exception {
		String id;
		Country country = new Country();
		country.setCountryCode(countryAddDTO.getCountryCode());
		country.setCountryName(countryAddDTO.getCountryName());
		country.setCreatedBy(countryAddDTO.getCreatedBy());
		country.setIsDeleted(false);
		try {
			id = countryDao.addCountry(country);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return id;
	}

	@Override
	public boolean updateCountry(CountryUpdateDTO countryDto) throws Exception {
		Country country = null;
		try {
			country = countryDao.getCountryById(countryDto.getCountryCode());
			if (null != country) {
//				country.setCountryId(countryDto.getCountryId());
				country.setCountryCode(countryDto.getCountryCode());
				country.setCountryName(countryDto.getCountryName());
				country.setModifiedBy(countryDto.getModifiedBy());
				country.setIsDeleted(false);
				countryDao.updateCountry(country);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	@Override
	public boolean deleteCountry(String code) throws Exception {
		Country country;
		try {
			country = countryDao.getCountryById(code);
			if (null != country) {
				country.setIsDeleted(true);
				countryDao.updateCountry(country);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	
	@Override
	public Long getCountriesCount() throws Exception {
		Long count;
		try {
			count = countryDao.getCountriesCount();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return count;
	}

}
