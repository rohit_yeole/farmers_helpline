/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.TalukaAddDTO;
import com.nath.dto.TalukaDTO;
import com.nath.dto.TalukaUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface TalukaService {

	public List<TalukaDTO> getTalukas(int page, int limit) throws Exception;

	public List<TalukaDTO> getTalukasByDistrictCode(String districtCode) throws Exception;

	public TalukaDTO getTalukaByCode(String code) throws Exception;

	public String addTaluka(TalukaAddDTO taluka) throws Exception;

	public boolean updateTaluka(TalukaUpdateDTO taluka) throws Exception;

	public boolean deleteTaluka(String code) throws Exception;

	public Long getTalukasCount() throws Exception;

}
