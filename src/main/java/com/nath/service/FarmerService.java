/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.FarmerAddDTO;
import com.nath.dto.FarmerDTO;
import com.nath.dto.FarmerSearchDTO;
import com.nath.dto.FarmerUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface FarmerService {

	public List<FarmerDTO> getFarmers(int page, int limit) throws Exception;

	public FarmerDTO getFarmerById(Integer id) throws Exception;

	public Integer addFarmer(FarmerAddDTO farmerAddDTO) throws Exception;

	public boolean updateFarmer(FarmerUpdateDTO farmerUpdateDTO) throws Exception;

	public boolean deleteFarmer(Integer id) throws Exception;

	public List<FarmerDTO> getFarmersByFilter(FarmerSearchDTO farmerSearchDTO) throws Exception;

	public Long getFarmersCount() throws Exception;
}
