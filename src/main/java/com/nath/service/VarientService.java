/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.VarientAddDTO;
import com.nath.dto.VarientDTO;
import com.nath.dto.VarientUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface VarientService {

	public List<VarientDTO> getVarients(int page, int limit) throws Exception;

	public List<VarientDTO> getVarientsByCropCode(String varientCode) throws Exception;

	public VarientDTO getVarientByCode(String code) throws Exception;

	public boolean updateVarient(VarientUpdateDTO varient) throws Exception;

	public String addVarient(VarientAddDTO varient) throws Exception;

	public boolean deleteVarient(String code) throws Exception;

	public Long getVarientsCount() throws Exception;;
}
