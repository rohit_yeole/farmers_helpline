/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.StateAddDTO;
import com.nath.dto.StateDTO;
import com.nath.dto.StateUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface StateService {

	public List<StateDTO> getStates(int page, int limit) throws Exception;

	public List<StateDTO> getStatesByCountryCode(String countryCode) throws Exception;

	public StateDTO getStateByCode(String code) throws Exception;

	public boolean updateState(StateUpdateDTO state) throws Exception;

	public String addState(StateAddDTO state) throws Exception;

	public boolean deleteState(String code) throws Exception;

	public Long getStatesCount() throws Exception;;
}
