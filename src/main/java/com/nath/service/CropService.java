/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.CropAddDTO;
import com.nath.dto.CropDTO;
import com.nath.dto.CropUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface CropService {

	public List<CropDTO> getCrops(int page, int limit) throws Exception;

	public CropDTO getCropByCode(String code) throws Exception;

	public String addCrop(CropAddDTO crop) throws Exception;

	public boolean updateCrop(CropUpdateDTO crop) throws Exception;

	public boolean deleteCrop(String code) throws Exception;

	public Long getCropsCount() throws Exception;
}
