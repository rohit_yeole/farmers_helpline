/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.BranchAddDTO;
import com.nath.dto.BranchDTO;
import com.nath.dto.BranchUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface BranchService {

	public List<BranchDTO> getBranches(int page, int limit) throws Exception;

	public BranchDTO getBranchByCode(String code) throws Exception;

	public String addBranch(BranchAddDTO branchAddDTO) throws Exception;

	public boolean updateBranch(BranchUpdateDTO branchUpdateDTO) throws Exception;

	public boolean deleteBranch(String code) throws Exception;

	public Long getBranchesCount() throws Exception;
}
