/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.VillageAddDTO;
import com.nath.dto.VillageDTO;
import com.nath.dto.VillageUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface VillageService {

	public List<VillageDTO> getVillages(int page, int limit) throws Exception;

	public VillageDTO getVillageByCode(String code) throws Exception;

	public String addVillage(VillageAddDTO villageAddDTO) throws Exception;

	public boolean updateVillage(VillageUpdateDTO villageUpdateDTO) throws Exception;

	public boolean deleteVillage(String code) throws Exception;

	public Long getVillagesCount() throws Exception;
}
