/**
 * 
 */
package com.nath.service;

import java.util.List;

import com.nath.dto.DistrictAddDTO;
import com.nath.dto.DistrictDTO;
import com.nath.dto.DistrictUpdateDTO;

/**
 * @author Rohit
 *
 */
public interface DistrictService {

	public List<DistrictDTO> getDistricts(int page, int limit) throws Exception;

	public List<DistrictDTO> getDistrictsByStateCode(String stateCode) throws Exception;

	public DistrictDTO getDistrictByCode(String code) throws Exception;

	public boolean updateDistrict(DistrictUpdateDTO district) throws Exception;

	public String addDistrict(DistrictAddDTO district) throws Exception;

	public boolean deleteDistrict(String code) throws Exception;

	public Long getDistrictsCount() throws Exception;
}
