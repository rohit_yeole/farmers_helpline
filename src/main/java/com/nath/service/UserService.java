package com.nath.service;

import java.util.List;

import com.nath.bo.MstUsers;
import com.nath.dto.UserAddDTO;
import com.nath.dto.UserDTO;
import com.nath.dto.UserUpdateDTO;
import com.nath.dto.UserValidateDTO;
import com.nath.dto.UserValidateResponseDTO;

public interface UserService {

	public UserValidateResponseDTO authenticateUser(UserValidateDTO validateDTO) throws Exception;

	public List<UserDTO> getUsers(int page, int limit) throws Exception;

	public UserDTO getUserById(int id) throws Exception;

	public Integer addUser(UserAddDTO country) throws Exception;

	public boolean updateUser(UserUpdateDTO country1) throws Exception;

	public boolean deleteUser(int id) throws Exception;

	public Long getUsersCount() throws Exception;

	public Boolean validateUsername(String username) throws Exception;

}
