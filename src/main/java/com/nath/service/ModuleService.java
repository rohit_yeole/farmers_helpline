package com.nath.service;

import java.util.List;

import com.nath.dto.ModuleAddDTO;
import com.nath.dto.ModuleUpdateDTO;
import com.nath.dto.ModulesDTO;

public interface ModuleService {

	public List<ModulesDTO> getModules(int page, int limit) throws Exception;

	public ModulesDTO getModuleById(int id) throws Exception;

	public String addModule(ModuleAddDTO module) throws Exception;

	public boolean updateModule(ModuleUpdateDTO module) throws Exception;

	public boolean deleteModule(int id) throws Exception;

	public Long getModulesCount() throws Exception;

}
